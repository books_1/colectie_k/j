# J

## Content

```
./J. A. Jance:
J. A. Jance - Reteaua raului 2.0 '{Thriller}.docx

./J. A. Redmerski:
J. A. Redmerski - The edge of - V1 Niciodata impreuna 1.0 '{Romance}.docx
J. A. Redmerski - The edge of - V2 Impreuna pentru totdeauna 1.0 '{Romance}.docx

./J. B. Charcot:
J. B. Charcot - Cristofor Columb vazut de un marinar 1.0 '{Istorie}.docx

./J. C. Pollock:
J. C. Pollock - Amenintarea 1.0 '{Suspans}.docx

./J. Clos:
J. Clos - Impostorii 1.0 '{Detectiv}.docx

./J. D. Horn:
J. D. Horn - Vraji in Savannah - V1 Bariera 1.0 '{Supranatural}.docx
J. D. Horn - Vraji in Savannah - V2 Sursa 1.0 '{Supranatural}.docx

./J. D. Robb:
J. D. Robb - Dezvaluiri imposibile 2.0 '{Romance}.docx
J. D. Robb - In Death - V02 Fara limite 0.8 '{Politista}.docx
J. D. Robb - In Death - V18 Ultima imagine 0.9 '{Politista}.docx
J. D. Robb - In Death - V22 Adulter 0.9 '{Politista}.docx
J. D. Robb - In Death - V23 In arsita noptii 0.99 '{Politista}.docx
J. D. Robb - In Death - V24 Martorul 0.9 '{Politista}.docx
J. D. Robb - In Death - V25 Razbunarea 0.7 '{Politista}.docx
J. D. Robb - In Death - V28 Afaceri murdare 1.0 '{Diverse}.docx
J. D. Robb - In Death - V29 Inocenta si crima 0.99 '{Politista}.docx
J. D. Robb - In Death - V32 Legaturi fatale 0.99 '{Politista}.docx
J. D. Robb - In Death - V33 Cupa cu venin 0.99 '{Politista}.docx
J. D. Robb - Sanctuarul 2.0 '{Diverse}.docx

./J. D. Sallinger:
J. D. Sallinger - Franny si Zoey 0.6 '{Literatura}.docx

./J. El Macho:
J. El Macho - Cautatorii de comori 2.0 '{Tineret}.docx

./J. H. Rosni Aine:
J. H. Rosni Aine - Leul urias 1.0 '{SF}.docx
J. H. Rosni Aine - Lupta pentru foc 3.0 '{SF}.docx

./J. H. Rosny Aine:
J. H. Rosny Aine - Navigatorii infinitului 1.0 '{SF}.docx

./J. K. Rowling:
J. K. Rowling - Moarte subita 2.0 '{AventuraTineret}.docx
J. K. Rowling - Povestile bardului Beedle 2.1 '{AventuraTineret}.docx
J. K. Rowling - V1 Harry Potter si piatra filozofala 5.0 '{AventuraTineret}.docx
J. K. Rowling - V2 Harry Potter si camera secretelor 5.0 '{AventuraTineret}.docx
J. K. Rowling - V3 Harry Potter si prizonierul din Azkaban 5.0 '{AventuraTineret}.docx
J. K. Rowling - V4 Harry Potter si pocalul de foc 5.0 '{AventuraTineret}.docx
J. K. Rowling - V5 Harry Potter si ordinul Phoenix 5.0 '{AventuraTineret}.docx
J. K. Rowling - V6 Harry Potter si printul semipur 5.0 '{AventuraTineret}.docx
J. K. Rowling - V7 Harry Potter si talismanele mortii 5.0 '{AventuraTineret}.docx

./J. L. Stratton:
J. L. Stratton - Empatie 1.0 '{SF}.docx

./J. M. Caballero Bonald:
J. M. Caballero Bonald - Agata ochi de pisica 1.0 '{Dragoste}.docx

./J. M. Coetzee:
J. M. Coetzee - Asteptandu-i pe barbari 1.0 '{Literatura}.docx
J. M. Coetzee - Copilaria lui Isus 0.99 '{Literatura}.docx
J. M. Coetzee - Dezonoare 0.99 '{Literatura}.docx
J. M. Coetzee - Epoca de fier 1.0 '{Literatura}.docx
J. M. Coetzee - Foe 1.0 '{Literatura}.docx
J. M. Coetzee - Miezul verii 1.0 '{Literatura}.docx

./J. M. G. Le Clezio:
J. M. G. Le Clezio - Potopul 0.9 '{SF}.docx

./J. N. Darby:
J. N. Darby - Disciplina 0.9 '{Religie}.docx

./J. P. Delaney:
J. P. Delaney - Fata dinainte 1.0 '{Politista}.docx

./J. Patrick Black:
J. Patrick Black - Ciocnirea Taramurilor - V1 Orasul noua in flacari 1.0 '{SF}.docx

./J. Pera:
J. Pera - Secretul donnei Dolores 2.0 '{Tineret}.docx

./J. R. R. Tolkien:
J. R. R. Tolkien - V0 Povesti neterminate 0.8 '{Aventura}.docx
J. R. R. Tolkien - V1 Hobbitul 4.0 '{Aventura}.docx
J. R. R. Tolkien - V2 Stapanul Inelelor - V0 Anexe 0.9 '{Aventura}.docx
J. R. R. Tolkien - V2 Stapanul Inelelor - V1 Fratia inelului 0.99 '{Aventura}.docx
J. R. R. Tolkien - V2 Stapanul Inelelor - V2 Cele doua turnuri 0.99 '{Aventura}.docx
J. R. R. Tolkien - V2 Stapanul Inelelor - V3 Intoarcerea regelui 0.99 '{Aventura}.docx
J. R. R. Tolkien - V3 Silmarillion 1.0 '{Aventura}.docx
J. R. R. Tolkien - V4 Copiii lui Hurin 2.0 '{Aventura}.docx

./J. R. Ward:
J. R. Ward - Fratia Pumnalului Negru - V1 Nopti de patima 1.0 '{Vampiri}.docx

./J. Rice:
J. Rice - Biblia in familie 0.8 '{Religie}.docx

./J. Rotte & K. Yamamoto:
J. Rotte & K. Yamamoto - Autovindecarea ochilor 0.5 '{Spiritualitate}.docx

./J. Rouw:
J. Rouw - Casa de aur 0.8 '{Religie}.docx

./Jack Black:
Jack Black - Programati-va succesul 0.8 '{DezvoltarePersonala}.docx

./Jack Bruce:
Jack Bruce - Umbra tigrului 1.0 '{ActiuneComando}.docx

./Jack Canfield:
Jack Canfield - Supa de pui pentru suflet 1.0 '{Spiritualitate}.docx
Jack Canfield - Supa de pui pentru suflet de femeie 1.0 '{Spiritualitate}.docx
Jack Canfield - Supa de pui pentru suflet de mama 1.0 '{Spiritualitate}.docx

./Jack Dann:
Jack Dann - Catedrala amintirilor 1.0 '{AventuraIstorica}.docx

./Jack Higgins:
Jack Higgins - Alarma in Malvine 1.0 '{ActiuneRazboi}.docx
Jack Higgins - Anul tigrului 1.0 '{ActiuneComando}.docx
Jack Higgins - Confesionalul 1.0 '{ActiuneComando}.docx
Jack Higgins - Ochiul furtunii 0.8 '{ActiuneComando}.docx
Jack Higgins - Pactul cu diavolul 0.99 '{ActiuneComando}.docx
Jack Higgins - Ultima sansa 1.0 '{ActiuneRazboi}.docx
Jack Higgins - Vulturul a aterizat 1.0 '{ActiuneRazboi}.docx
Jack Higgins - Zborul vulturilor 1.0 '{ActiuneRazboi}.docx

./Jack Hild:
Jack Hild - Alarma de gradul zero 0.9 '{ActiuneComando}.docx
Jack Hild - Capcana pentru colonel 1.0 '{ActiuneComando}.docx
Jack Hild - Infern in Iran 1.0 '{ActiuneComando}.docx
Jack Hild - Revansa 1.0 '{ActiuneComando}.docx

./Jack Hillman:
Jack Hillman - Misiune de Cercetare 1.0 '{SF}.docx

./Jackie Collins:
Jackie Collins - Celebritate 1.0 '{Romance}.docx
Jackie Collins - Cocota - Tandrete regasita 0.99 '{Romance}.docx
Jackie Collins - Infidelitati la Hollywood 0.8 '{Romance}.docx
Jackie Collins - Ispita 1.0 '{Romance}.docx
Jackie Collins - Jocuri periculoase 1.0 '{Romance}.docx
Jackie Collins - Zeita razbunarii 1.0 '{Romance}.docx

./Jackie Reemick:
Jackie Reemick - Barbatul misterios 0.99 '{Dragoste}.docx

./Jack Kerouac:
Jack Kerouac - Pe drum 0.9 '{Diverse}.docx

./Jack Lance:
Jack Lance - Pirofobia 1.0 '{Literatura}.docx

./Jack London:
Jack London - Alcool 0.9 '{Aventura}.docx
Jack London - Aventuri in Alaska 0.6 '{Aventura}.docx
Jack London - Calcaiul de fier 2.0 '{Aventura}.docx
Jack London - Chemarea strabunilor 1.0 '{Aventura}.docx
Jack London - Ciuma stacojie 1.0 '{Aventura}.docx
Jack London - Colt alb 2.0 '{Aventura}.docx
Jack London - Dragoste de viata 0.9 '{Aventura}.docx
Jack London - Fiul lupului 2.0 '{Aventura}.docx
Jack London - Inainte de Adam 1.0 '{Aventura}.docx
Jack London - Insula canibalilor 1.0 '{Aventura}.docx
Jack London - La capatul canionului 1.0 '{Aventura}.docx
Jack London - Martin Eden 1.0 '{Aventura}.docx
Jack London - Opere Alese V1 1.0 '{Aventura}.docx
Jack London - Opere Alese V2 0.9 '{Aventura}.docx
Jack London - Opere Alese V3 1.0 '{Aventura}.docx
Jack London - Patrula de pescuit 1.0 '{Aventura}.docx
Jack London - Povestiri 0.7 '{Aventura}.docx
Jack London - Ratacitor printre stele 1.0 '{Aventura}.docx
Jack London - Revolta pe Atlantic 1.0 '{Aventura}.docx
Jack London - Smoke Bellew 2.0 '{Aventura}.docx
Jack London - Spovedania unui betiv 0.99 '{Aventura}.docx
Jack London - Trei inimi 1.0 '{Aventura}.docx
Jack London - Zapezi insangerate 2.0 '{Aventura}.docx

./Jack Sharkey:
Jack Sharkey - Twerlikul 0.8 '{SF}.docx

./Jack Skillingstead:
Jack Skillingstead - Viata la pastrare 1.0 '{Diverse}.docx

./Jack Vance:
Jack Vance - Abominabilul Mcinch 0.9 '{SF}.docx
Jack Vance - Clubul de vacanta stelar 0.9 '{SF}.docx
Jack Vance - Lovitura de gratie 0.99 '{SF}.docx
Jack Vance - Lumile lui Magnus Ridolph 2.0 '{SF}.docx
Jack Vance - Molia lunii 0.9 '{SF}.docx
Jack Vance - Razboinicii de pe Kokod 0.9 '{SF}.docx
Jack Vance - Regele hotilor 0.9 '{SF}.docx
Jack Vance - Urlatorii 0.9 '{SF}.docx

./Jack Whyte:
Jack Whyte - Ordinul Templierilor - V1 Cavalerii in alb si negru 1.0 '{AventuraIstorica}.docx
Jack Whyte - Ordinul Templierilor - V2 Codul onoarei V1 1.0 '{AventuraIstorica}.docx
Jack Whyte - Ordinul Templierilor - V2 Codul onoarei V2 1.0 '{AventuraIstorica}.docx

./Jack Williamson:
Jack Williamson - Mai intunecat decat crezi 1.0 '{Supranatural}.docx

./Jacqueline Baird:
Jacqueline Baird - Copilul de Sf. Valentin 1.0 '{Literatura}.docx

./Jacqueline Case:
Jacqueline Case - Ce poate face dragostea 0.9 '{Romance}.docx
Jacqueline Case - Prea putine amintiri 0.9 '{Dragoste}.docx

./Jacqueline Hunt:
Jacqueline Hunt - Condamnat la iubire 0.99 '{Dragoste}.docx

./Jacqueline Monsigny:
Jacqueline Monsigny - V1 Floris, dragostea mea 4.0 '{Dragoste}.docx
Jacqueline Monsigny - V2 Floris, cavalerul de Petersburg 1.0 '{Dragoste}.docx
Jacqueline Monsigny - V3 Frumoasa din Louisiana 1.0 '{Dragoste}.docx
Jacqueline Monsigny - V4 Amantii de pe Mississippi 4.0 '{Dragoste}.docx

./Jacques Barcia:
Jacques Barcia - Portrete 0.8 '{SF}.docx

./Jacques Bergier:
Jacques Bergier - Cartile blestemate 1.0 '{MistersiStiinta}.docx

./Jacques Chessex:
Jacques Chessex - Capcaunul 0.9 '{Diverse}.docx

./Jacques de Launay:
Jacques de Launay - Mari decizii ale celui de-al doilea razboi mondial V1 1.0 '{Istorie}.docx
Jacques de Launay - Mari decizii ale celui de-al doilea razboi mondial V2 1.0 '{Istorie}.docx
Jacques de Launay - Ultimele zile ale fascismului in Europa 0.9 '{Istorie}.docx

./Jacques Futrelle:
Jacques Futrelle - Stapanul diamantelor 1.0 '{Thriller}.docx

./Jacques Henri Bernardin de Saint Pierre:
Jacques Henri Bernardin de Saint Pierre - Paul si Virginia 1.0 '{Dragoste}.docx

./Jacques Salome:
Jacques Salome - Curajul de a fi tu insuti 0.9 '{Psihologie}.docx
Jacques Salome - Daca m-as asculta m-as intelege 1.0 '{Psihologie}.docx

./Jacques Serbanesco:
Jacques Serbanesco - Donbas 0.7 '{Istorie}.docx

./Jacques Stephen Alexis:
Jacques Stephen Alexis - Inspectorul de fantasme 1.0 '{SF}.docx

./Jacques Vigne:
Jacques Vigne - Meditatie si psihologie 0.8 '{Spiritualitate}.docx

./Jaime Bayly:
Jaime Bayly - Escrocul sentimental 0.99 '{Romance}.docx

./Jakob Lorber:
Jakob Lorber - Calea renasterii 0.9 '{Spiritualitate}.docx
Jakob Lorber - Cartea Domnului despre viata si sanatate 0.9 '{Spiritualitate}.docx
Jakob Lorber - Casa Domnului V1 0.99 '{Spiritualitate}.docx
Jakob Lorber - Casa Domnului V2 0.99 '{Spiritualitate}.docx
Jakob Lorber - Casa Domnului V3 0.99 '{Spiritualitate}.docx
Jakob Lorber - Casa Domnului V4 0.99 '{Spiritualitate}.docx
Jakob Lorber - Casa Domnului V5 0.99 '{Spiritualitate}.docx
Jakob Lorber - Cele trei zile petrecute de Iisus la templu 0.99 '{Spiritualitate}.docx
Jakob Lorber - Copilaria lui Iisus 0.99 '{Spiritualitate}.docx
Jakob Lorber - Copilaria si adolescenta lui Iisus 0.99 '{Spiritualitate}.docx
Jakob Lorber - Cuvantul divin dat de Iisus lui Jacob Lorber 0.9 '{Spiritualitate}.docx
Jakob Lorber - De la iad la rai V1 0.99 '{Spiritualitate}.docx
Jakob Lorber - De la iad la rai V2 0.99 '{Spiritualitate}.docx
Jakob Lorber - De la iad la rai V3 0.99 '{Spiritualitate}.docx
Jakob Lorber - Dincolo de prag 0.99 '{Spiritualitate}.docx
Jakob Lorber - Evanghelia muntilor 0.99 '{Spiritualitate}.docx
Jakob Lorber - Evanghelia Pacii a lui Ioan 0.99 '{Spiritualitate}.docx
Jakob Lorber - Imparatia copiilor din lumea de apoi 0.99 '{Spiritualitate}.docx
Jakob Lorber - Imparatia misterioasa a spiritelor - Episcopul Martin var.1 0.9 '{Spiritualitate}.docx
Jakob Lorber - Intalnirea cu Christos 0.9 '{Spiritualitate}.docx
Jakob Lorber - Interpretarea Scripturilor 0.99 '{Spiritualitate}.docx
Jakob Lorber - Luna 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V1 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V2 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V3 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V4 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V5 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V6 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V7 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V8 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V9 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V10 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marea Evanghelie a lui Ioan V11 0.99 '{Spiritualitate}.docx
Jakob Lorber - Marturii despre natura 0.99 '{Spiritualitate}.docx
Jakob Lorber - Musca 0.99 '{Spiritualitate}.docx
Jakob Lorber - O evanghelie a muntilor 0.99 '{Spiritualitate}.docx
Jakob Lorber - Pamantul 0.99 '{Spiritualitate}.docx
Jakob Lorber - Puterea de vindecare a luminii Soarelui 0.99 '{Spiritualitate}.docx
Jakob Lorber - Saturn 0.99 '{Spiritualitate}.docx
Jakob Lorber - Scrisorile necunoscute ale lui Iisus Cristos catre Abgar Ukkama, regele Edessei 0.9 '{Spiritualitate}.docx
Jakob Lorber - Textele biblice si sensul lor tainic 0.99 '{Spiritualitate}.docx

./Jakob Lorber & Gottfried Mayerhofer:
Jakob Lorber & Gottfried Mayerhofer - A doua venire a lui Christos pe Pamant 0.99 '{Spiritualitate}.docx

./Jakob Vedelsby:
Jakob Vedelsby - Legea umanitatii 1.0 '{Literatura}.docx

./Jakob Wassermann:
Jakob Wassermann - Cazul Maurizius 1.0 '{Politista}.docx

./James A. Owen:
James A. Owen - Aici, in taramul dragonilor 1.0 '{AventuraTineret}.docx

./James Aldridge:
James Aldridge - Vanatorul 1.0 '{Literatura}.docx

./James Allen:
James Allen - Asa cum gandeste omul 0.9 '{DezvoltarePersonala}.docx

./James Barlow:
James Barlow - Personalul de 1 si 6 min 0.7 '{Politista}.docx

./James Becker:
James Becker - Primul apostol 1.0 '{Thriller}.docx

./James Clavell:
James Clavell - V1 Changi 1.0 '{AventuraIstorica}.docx
James Clavell - V2 Tai Pan 2.0 '{AventuraIstorica}.docx
James Clavell - V3 Shogun 1.0 '{AventuraIstorica}.docx
James Clavell - V4 Nobila Casa - V1 2.0 '{AventuraIstorica}.docx
James Clavell - V4 Nobila Casa - V2 2.0 '{AventuraIstorica}.docx
James Clavell - V4 Nobila Casa - V3 Justin Scott - Cei noua dragoni 1.0 '{AventuraIstorica}.docx
James Clavell - V5 Vartejul V1 0.9 '{AventuraIstorica}.docx
James Clavell - V5 Vartejul V2 0.9 '{AventuraIstorica}.docx
James Clavell - V6 Gai Jin 2.0 '{AventuraIstorica}.docx
James Clavell - V7 Evadarea 1.0 '{AventuraIstorica}.docx

./James Corey:
James Corey - Expansiunea - V1 Trezirea leviatanului 1.0 '{SF}.docx
James Corey - Expansiunea - V2 Razboiul lui Caliban 1.0 '{SF}.docx
James Corey - Expansiunea - V3 Poarta lui Abaddon 1.0 '{SF}.docx

./James Dashner:
James Dashner - Captiv in Labirint - V1 Captiv in labirint 2.0 '{SF}.docx
James Dashner - Captiv in Labirint - V2 Incercarile focului 2.0 '{SF}.docx
James Dashner - Captiv in Labirint - V3 Tratament letal 2.0 '{SF}.docx
James Dashner - Doctrina Mortala - V1 Cu ochii mintii 1.0 '{SF}.docx
James Dashner - Doctrina Mortala - V2 Controlul gandurilor 1.0 '{SF}.docx

./James E. Gunn:
James E. Gunn - Marea asteptare 0.9 '{SF}.docx

./James Eliroy:
James Eliroy - Dulcele suras al Daliei 1.0 '{Thriller}.docx

./James Ellroy:
James Ellroy - Dulcele sarut al Daliei 1.0 '{Thriller}.docx
James Ellroy - Marele niciunde 1.0 '{Thriller}.docx

./James Fenimore Cooper:
James Fenimore Cooper - Corsarul rosu 2.0 '{Western}.docx
James Fenimore Cooper - Cuceritorii Pacificului 1.0 '{Western}.docx
James Fenimore Cooper - Leii de mare 2.0 '{Western}.docx
James Fenimore Cooper - Preeria - V1 Pionerii 1.0 '{Western}.docx
James Fenimore Cooper - Preeria - V2 Ultimul mohican 1.0 '{Western}.docx
James Fenimore Cooper - Preeria - V3 Preeria V1 1.1 '{Western}.docx
James Fenimore Cooper - Preeria - V3 Preeria V2 1.1 '{Western}.docx
James Fenimore Cooper - Preeria - V4 Calauza 1.0 '{Western}.docx
James Fenimore Cooper - Preeria - V4 Vanatorul de cerbi 1.0 '{Western}.docx
James Fenimore Cooper - Vanatorul 1.0 '{Western}.docx

./James Frey:
James Frey - O mie de farame 1.0 '{Literatura}.docx
James Frey - Prietenul meu Leonard 1.0 '{Literatura}.docx

./James Frey & Nils Johnson Shelton:
James Frey & Nils Johnson Shelton - Endgame - V1 Convocarea 1.0 '{SF}.docx
James Frey & Nils Johnson Shelton - Endgame - V2 Cheia cerului 1.0 '{SF}.docx
James Frey & Nils Johnson Shelton - Endgame - V3 Regulile jocului 1.0 '{SF}.docx

./James G. Ballard:
James G. Ballard - Orasul inchis 0.99 '{SF}.docx

./James Grady:
James Grady - Caini turbati 0.9 '{ActiuneRazboi}.docx

./James Graham Ballard:
James Graham Ballard - Imperiul soarelui 1.0 '{ActiuneRazboi}.docx

./James Hadley Chase:
James Hadley Chase - Am lumea in buzunar 1.0 '{Politista}.docx
James Hadley Chase - Asul din pachet 1.0 '{Politista}.docx
James Hadley Chase - Blondele sunt moartea mea 1.0 '{Politista}.docx
James Hadley Chase - Cadavre la minut 1.0 '{Politista}.docx
James Hadley Chase - Ca la teatru 2.0 '{Politista}.docx
James Hadley Chase - Careul de asi 1.0 '{Politista}.docx
James Hadley Chase - Considera-te mort 2.0 '{Politista}.docx
James Hadley Chase - Cosmarul 1.0 '{Politista}.docx
James Hadley Chase - Culcati-o intre crini 0.99 '{Politista}.docx
James Hadley Chase - Fa-mi placerea si crapa! 1.0 '{Politista}.docx
James Hadley Chase - Fie-i tarana usoara 2.0 '{Politista}.docx
James Hadley Chase - Inscenarea 2.0 '{Politista}.docx
James Hadley Chase - Mai bine sarac 1.0 '{Politista}.docx
James Hadley Chase - Misiune la Venetia 0.99 '{Politista}.docx
James Hadley Chase - Moarte de calibrul 45 1.0 '{Politista}.docx
James Hadley Chase - Mortii nu vorbesc 1.0 '{Politista}.docx
James Hadley Chase - Nici o orhidee pentru Miss Blandish 1.0 '{Politista}.docx
James Hadley Chase - O adunatura de lichele 1.0 '{Politista}.docx
James Hadley Chase - Orhideea ucigasa 0.9 '{Politista}.docx
James Hadley Chase - Parsivele de femei 0.99 '{Politista}.docx
James Hadley Chase - Pumnalul cu diamante 1.0 '{Politista}.docx
James Hadley Chase - Rabdatoare pasari, vulturii 2.0 '{Politista}.docx
James Hadley Chase - Schimbare de decor 1.0 '{Politista}.docx
James Hadley Chase - Si cu mine ce o sa se intample 1.0 '{Politista}.docx
James Hadley Chase - Sunetul banilor 1.0 '{Politista}.docx
James Hadley Chase - Ti-arat eu 0.99 '{Politista}.docx
James Hadley Chase - Totul se plateste 1.0 '{Politista}.docx
James Hadley Chase - Un hippie pe autostrada 1.0 '{Politista}.docx

./James Herbert:
James Herbert - Sobolanii 1.0 '{Horror}.docx

./James Joyce:
James Joyce - Oamenii din Dublin 0.9 '{Diverse}.docx
James Joyce - Portret al artistului in tinerete 0.9 '{Diverse}.docx

./James L. Cambias:
James L. Cambias - Oceanul orbilor 1.0 '{SF}.docx

./James L. Garlow & Peter Jones:
James L. Garlow & Peter Jones - Codul spart al lui Da Vinci 1.0 '{Religie}.docx

./James Lee Burke:
James Lee Burke - Dave Robicheaux - V1 Ploaia electrica 1.0 '{Literatura}.docx

./James Levine:
James Levine - Jurnalul albastru 1.0 '{Literatura}.docx

./James M. Cain:
James M. Cain - Asigurare pentru moarte 2.0 '{Politista}.docx
James M. Cain - Postasul suna intotdeauna de doua ori 2.0 '{Politista}.docx

./James Mcconnell:
James Mcconnell - Situatie de evitare 0.99 '{SF}.docx

./James Meek:
James Meek - Incepem coborarea 0.99 '{Literatura}.docx
James Meek - Invaziile inimii 0.9 '{Literatura}.docx
James Meek - Un gest de iubire 1.0 '{Literatura}.docx

./James Oliver Curwood:
James Oliver Curwood - Bari cainele lup 4.0 '{Aventura}.docx
James Oliver Curwood - Grizzly stapanul muntilor 2.0 '{Aventura}.docx
James Oliver Curwood - Nomazii nordului 2.0 '{Aventura}.docx
James Oliver Curwood - Nordul salbatec 1.0 '{Aventura}.docx
James Oliver Curwood - Taina ghetarilor 1.0 '{Aventura}.docx
James Oliver Curwood - Vanatorul de oameni 1.0 '{Aventura}.docx

./James Patrick Kelly:
James Patrick Kelly - Barbatii aduc numai necazuri 0.9 '{SF}.docx
James Patrick Kelly - Foc 0.7 '{SF}.docx
James Patrick Kelly - Prizonierul din Chillon 0.9 '{SF}.docx
James Patrick Kelly - Stand in rand cu Mister Jimmy 0.99 '{SF}.docx

./James Patterson:
James Patterson - Alerta de gradul 0 2.1 '{Thriller}.docx
James Patterson - Alex Cross - V3 Jack & Jill 1.0 '{Thriller}.docx
James Patterson - Alex Cross - V5 Marea ratare 1.0 '{Thriller}.docx
James Patterson - Alex Cross - V6 Trandafirii sunt rosii 1.0 '{Thriller}.docx
James Patterson - Alex Cross - V7 Violetele sunt albastre 1.0 '{Thriller}.docx
James Patterson - Alex cross - V8 Inscenarea 1.0 '{Thriller}.docx
James Patterson - Alex Cross - V11 Lista neagra 1.0 '{Thriller}.docx
James Patterson - Alex Cross - V19 Detectivul Alex Cross 1.0 '{Thriller}.docx
James Patterson - Antologie Thriller V1 1.0 '{Thriller}.docx
James Patterson - Antologie Thriller V2 1.0 '{Thriller}.docx
James Patterson - Casa de pe plaja 1.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V1 1-ul pe lista mortii 2.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V2 A 2-a sansa 2.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V3 Al 3-lea caz 2.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V4 4 iulie 2.1 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V5 Al 5-lea calaret 2.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V6 A 6-a tinta 1.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V7 Al 7-lea cer 1.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V8 A 8-a marturisire 1.0 '{Thriller}.docx
James Patterson - Clubul Fetelor de la Crime - V9 A 9-a judecata 1.0 '{Thriller}.docx
James Patterson - De-a v-ati ascunselea 1.0 '{Thriller}.docx
James Patterson - Invizibil 1.0 '{Thriller}.docx
James Patterson - Maximum Ride - V1 Experimentul Angel 0.7 '{Thriller}.docx
James Patterson - Maximum Ride - V2 Scoala s-a terminat pe veci 1.0 '{Thriller}.docx
James Patterson - Maximum Ride - V3 Salvarea lumii si alte sporturi extreme 1.0 '{Thriller}.docx
James Patterson - Private - Agentia de investigatii 1.0 '{Thriller}.docx
James Patterson - Soarecele si pisica 1.0 '{Thriller}.docx
James Patterson - Vrajitori & Vrajitoare - V1 Vrajitori & Vrajitoare 1.0 '{Thriller}.docx
James Patterson - Vrajitori & Vrajitoare - V2 Darul 1.0 '{Thriller}.docx

./James Patterson & Andrew Cross:
James Patterson & Andrew Cross - Bufonul 1.0 '{Thriller}.docx

./James Patterson & Martin Dugard:
James Patterson & Martin Dugard - Asasinarea lui Tutankhamon 1.0 '{Thriller}.docx

./James Rayburn:
James Rayburn - Adevarul absolut 1.0 '{Politista}.docx

./James Redfield:
James Redfield - Celestine - V1 Profetiile de la Celestine 1.0 '{Spiritualitate}.docx
James Redfield - Celestine - V2 A zecea viziune 0.9 '{Spiritualitate}.docx
James Redfield - Celestine - V3 Secretul Shambhalei 1.1 '{Spiritualitate}.docx
James Redfield - Celestine - V4 Mentinerea viziunii 0.7 '{Spiritualitate}.docx

./James Rollins:
James Rollins - Sigma Force - V1 Furtuna de nisip 1.0 '{Aventura}.docx
James Rollins - Sigma Force - V2 Harta oaselor 1.0 '{Aventura}.docx
James Rollins - Sigma Force - V3 Ordinul negru 1.0 '{Aventura}.docx
James Rollins - Sigma Force - V4 Virusul lui Iuda 1.0 '{Aventura}.docx

./James Tiptree Jr.:
James Tiptree Jr. - Houston, Houston, ma auzi 0.99 '{SF}.docx
James Tiptree Jr. - Intoarcerea acasa 0.99 '{SF}.docx
James Tiptree Jr. - Iubire e planul, planul e moarte 0.9 '{SF}.docx

./James Twining:
James Twining - Soarele negru 1.0 '{Suspans}.docx
James Twining - Vulturul cu doua capete 2.0 '{Suspans}.docx

./James Twyman:
James Twyman - Curs de indoit linguri V1 0.2 '{Spiritualitate}.docx

./James Van Pelt:
James Van Pelt - Ultima dintre formele O 1.0 '{SF}.docx

./James W. Porter:
James W. Porter - Teroare in Emirau 1.0 '{ActiuneComando}.docx

./Jamie Bartlett:
Jamie Bartlett - Oameni versus Tehnologie 1.0 '{MistersiStiinta}.docx

./Jamie King:
Jamie King - 111 teorii ale conspiratiei 1.0 '{MistersiStiinta}.docx

./Jamie McGuire:
Jamie McGuire - Fericirea mea esti tu 0.9 '{Romance}.docx

./Jamie Mcguire:
Jamie Mcguire - Fericirea incepe azi 1.0 '{Romance}.docx

./Jandy Nelson:
Jandy Nelson - Cerul e pretutindeni 0.99 '{Literatura}.docx

./Jane Arbor:
Jane Arbor - Atat de frumoasa era noaptea 1.0 '{Romance}.docx

./Jane Ashton:
Jane Ashton - Partener de afaceri 1.0 '{Romance}.docx
Jane Ashton - Un moment de nebunie 1.0 '{Romance}.docx
Jane Ashton - Vacanta prelungita 0.9 '{Romance}.docx

./Jane Austen:
Jane Austen - Elinor si Marianne 1.0 '{ClasicSt}.docx
Jane Austen - Emma 1.0 '{ClasicSt}.docx
Jane Austen - Manastirea Northanger 1.0 '{ClasicSt}.docx
Jane Austen - Mandrie si prejudecata 2.0 '{ClasicSt}.docx
Jane Austen - Ratiune si simtire 0.9 '{ClasicSt}.docx

./Jane Blackmore:
Jane Blackmore - Singura impotriva tuturor 0.99 '{Dragoste}.docx

./Jane Bonander:
Jane Bonander - Dormitorul este al meu 0.99 '{Romance}.docx

./Jane Costello:
Jane Costello - Aproape casatoriti 1.0 '{Romance}.docx

./Jane East:
Jane East - Insotitoarea 0.99 '{Dragoste}.docx

./Jane Fallon:
Jane Fallon - Dulcea mea razbunare 1.0 '{Romance}.docx

./Jane Harper:
Jane Harper - Arsita 1.0 '{Thriller}.docx

./Janet Dailey:
Janet Dailey - Mahon de vara 1.0 '{Romance}.docx
Janet Dailey - Nedoritul 1.0 '{Romance}.docx

./Janet Nica:
Janet Nica - Fat Frumos din lacrima de paradox 0.8 '{Diverse}.docx

./Janet Wellington:
Janet Wellington - Spune-mi ca ma iubesti 0.99 '{Romance}.docx

./Janet Whitehead:
Janet Whitehead - Fagaduiala Orientului 0.99 '{Dragoste}.docx
Janet Whitehead - Goana dupa aur 0.7 '{Dragoste}.docx

./Jane Van Lawick Goodal:
Jane Van Lawick Goodal - In umbra omului 2.0 '{MistersiStiinta}.docx

./Jane Wallace:
Jane Wallace - O cariera de succes 0.99 '{Dragoste}.docx
Jane Wallace - Pretul succesului 0.99 '{Dragoste}.docx

./Jane Woods:
Jane Woods - Casuta de pe deal 0.9 '{Dragoste}.docx
Jane Woods - Dragoste din intamplare 0.99 '{Dragoste}.docx

./Jane Yolen:
Jane Yolen - Tesatoarea viselor 0.8 '{SF}.docx

./Jan Guillou:
Jan Guillou - Madame Terror 0.99 '{Suspans}.docx
Jan Guillou - Raul 1.0 '{Literatura}.docx

./Jan Haye:
Jan Haye - Infirmiera la castel 0.99 '{Dragoste}.docx

./Jan Hudson:
Jan Hudson - Doi barbati si o femeie 0.99 '{Dragoste}.docx
Jan Hudson - Umbra noptii 0.7 '{Dragoste}.docx

./Janice Bartlett:
Janice Bartlett - Salvamarul 0.99 '{Dragoste}.docx

./Janice Maynard:
Janice Maynard - Casa vrajita 0.9 '{Dragoste}.docx
Janice Maynard - Valea cascadelor 0.2 '{Dragoste}.docx

./Jan Kozac:
Jan Kozac - Mariana 1.0 '{Diverse}.docx

./Jan Martenson:
Jan Martenson - Aurul dragonului 1.0 '{Politista}.docx
Jan Martenson - Moartea viziteaza vrajitoarele 1.0 '{Politista}.docx
Jan Martenson - Natura moarta pentru flasneta mecanica 1.0 '{Politista}.docx

./Jan Otcenasek:
Jan Otcenasek - Pe cand in rai ploua 2.0 '{Dragoste}.docx
Jan Otcenasek - Romeo, Julieta si intunericul 1.0 '{Dragoste}.docx

./Jan Phillip Sendker:
Jan Phillip Sendker - Soapta inimii 1.0 '{Romance}.docx

./Jan Scarbrough:
Jan Scarbrough - Cele mai bune intentii 0.9 '{Dragoste}.docx

./Jan Van Helsing:
Jan Van Helsing - Organizatiile secrete si puterea lor in secolul XX 0.9 '{MistersiStiinta}.docx

./Jan Wallentin:
Jan Wallentin - Steaua lui Strindberg 1.0 '{Literatura}.docx

./Jan Weiss:
Jan Weiss - Casa cu o mie de etaje 1.0 '{SF}.docx

./Jaques de Chambon:
Jaques de Chambon - Fugara 2.0 '{Western}.docx

./Jaques Perry:
Jaques Perry - Insula altuia 1.0 '{Aventura}.docx

./Jared Cade:
Jared Cade - Razbunare mortala 1.0 '{Literatura}.docx

./Jaroslav Hasek:
Jaroslav Hasek - Peripetiile bravului soldat Svejk 1.1 '{Tineret}.docx
Jaroslav Hasek - Peripetiile bravului soldat Svejk V1 2.0 '{Tineret}.docx
Jaroslav Hasek - Peripetiile bravului soldat Svejk V2 2.0 '{Tineret}.docx
Jaroslav Hasek - Peripetiile bravului soldat Svejk V3 2.0 '{Tineret}.docx

./Jaroslav Kalfar:
Jaroslav Kalfar - Astronautul din Boemia 1.0 '{Literatura}.docx

./Jaroslav Veis:
Jaroslav Veis - Sanatate 0.99 '{SF}.docx

./Jasinda Wilder:
Jasinda Wilder - Ma pierd in tine 0.7 '{Romance}.docx
Jasinda Wilder - Ma regasesc in noi 0.9 '{Romance}.docx
Jasinda Wilder - Raniti 1.0 '{Romance}.docx

./Jasmine Craig:
Jasmine Craig - Asa a fost scris in stele 0.9 '{Romance}.docx
Jasmine Craig - Intoarcere la Wharuaroa 0.9 '{Dragoste}.docx

./Jasmine Warga:
Jasmine Warga - Inima mea si alte gauri negre 1.0 '{Literatura}.docx

./Jasmuheen:
Jasmuheen - A trai cu lumina 0.99 '{Spiritualitate}.docx
Jasmuheen - Biocampuri personale 1.0 '{Spiritualitate}.docx
Jasmuheen - Biocampuri si extaz - V3 Frecventa Madonei 0.99 '{Spiritualitate}.docx
Jasmuheen - Despre meditatie 1.0 '{Spiritualitate}.docx
Jasmuheen - Dezvoltarea constientei umane 1.0 '{Spiritualitate}.docx
Jasmuheen - Hrana zeilor 1.0 '{Spiritualitate}.docx
Jasmuheen - In rezonanta 0.7 '{Spiritualitate}.docx
Jasmuheen - Instrumente & Tao 0.9 '{Spiritualitate}.docx
Jasmuheen - Mantra lui Dalai Lama 0.7 '{Spiritualitate}.docx
Jasmuheen - Radianta divina 0.7 '{Spiritualitate}.docx
Jasmuheen - Sambata dimineata, noiembrie 2003 0.2 '{Spiritualitate}.docx
Jasmuheen - Sanatatea celor patru corpuri 0.7 '{Spiritualitate}.docx
Jasmuheen - Vineri seara, noiembrie 2003 0.2 '{Spiritualitate}.docx

./Jason Dark:
Jason Dark - Spaima deasupra Londrei 1.0 '{Horror}.docx

./Jason Goodwin:
Jason Goodwin - Copacul ienicerilor 1.0 '{Literatura}.docx

./Jason Matthews:
Jason Matthews - Red Sparrow - V1 Vrabia rosie 1.0 '{Razboi}.docx
Jason Matthews - Red Sparrow - V2 Palatul tradarilor 1.0 '{Razboi}.docx
Jason Matthews - Red Sparrow - V3 Candidatul Kremlinului 1.0 '{Razboi}.docx

./Jasper Fforde:
Jasper Fforde - Cazul Jane Eyre 0.8 '{SF}.docx

./Jaume Cabre:
Jaume Cabre - Umbra eunucului 0.9 '{Diverse}.docx

./Javier Cercas:
Javier Cercas - Legile frontierei 1.0 '{Literatura}.docx
Javier Cercas - Viteza luminii 0.9 '{Literatura}.docx

./Javier Marias:
Javier Marias - Barbatul sentimental 1.0 '{Literatura}.docx
Javier Marias - Chipul tau, Maine - V1 Febra. Lance 1.0 '{Literatura}a.docx
Javier Marias - Chipul tau, Maine - V2 Dans. Vis 1.0 '{Literatura}.docx
Javier Marias - Indragostirile 1.0 '{Literatura}.docx
Javier Marias - Inima atat de alba 1.0 '{Literatura}.docx
Javier Marias - Maine in batalie sa te gandesti la mine 1.0 '{Literatura}.docx
Javier Marias - Romanul Oxfordului 0.99 '{Literatura}.docx

./Javier Sierra:
Javier Sierra - Cina secreta 2.0 '{AventuraIstorica}.docx
Javier Sierra - Ingerul pierdut 1.0 '{AventuraIstorica}.docx

./Jayant Vishnu Narlikar:
Jayant Vishnu Narlikar - Cometa 1.0 '{SF}.docx

./Jay Hosking:
Jay Hosking - Trei ani in labirint 1.0 '{Diverse}.docx

./Jaymi Cristol:
Jaymi Cristol - Trei dorinte 1.0 '{Romance}.docx

./Jayne Ann Krentz:
Jayne Ann Krentz - Crede-ma 1.0 '{Romance}.docx
Jayne Ann Krentz - Eclipse Bay - V1 Intoarcerea in Eclipse Bay 1.0 '{Romance}.docx
Jayne Ann Krentz - Eclipse Bay - V2 Noi inceputuri in Eclipse Bay 1.0 '{Romance}.docx
Jayne Ann Krentz - Eclipse Bay - V3 O vara in Eclipse Bay 1.0 '{Romance}.docx
Jayne Ann Krentz - Meandre 1.0 '{Romance}.docx
Jayne Ann Krentz - Whispering Springs - V2 Spaimele trecutului 1.0 '{Romance}.docx

./Jayne Castle:
Jayne Castle - Vrajitoarea florilor 1.0 '{Romance}.docx

./Jayne Holloway:
Jayne Holloway - Fara mila 0.99 '{Romance}.docx
Jayne Holloway - Vacanta de vis 0.9 '{Romance}.docx

./Jay Werkheiser:
Jay Werkheiser - Drone 1.0 '{SF}.docx

./Jean:
Jean-Christophe Grange - Rauri de purpura 1.0 '{Thriller}.docx
Jean-Christophe Grange - Taramul mortilor 1.0 '{Thriller}.docx

./Jean Bart:
Jean Bart - Cartea Dunarii 0.9 '{ClasicRo}.docx
Jean Bart - Datorii uitate 1.0 '{ClasicRo}.docx
Jean Bart - Europolis1.0 '{ClasicRo}.docx
Jean Bart - In cusca leului 1.0 '{ClasicRo}.docx
Jean Bart - Jurnal de bord. Schite marine 1.2 '{ClasicRo}.docx
Jean Bart - Jurnal de bord si altele 1.0 '{ClasicRo}.docx
Jean Bart - Peste ocean 1.2 '{ClasicRo}.docx
Jean Bart - Printesa Bibita 1.0 '{ClasicRo}.docx
Jean Bart - Rusia si Basarabia 0.9 '{ClasicRo}.docx

./Jean Beaufret:
Jean Beaufret - Lectii de filozofie 0.99 '{Filozofie}.docx

./Jean Bourdier:
Jean Bourdier - Comandourile desertului 1.0 '{ActiuneComando}.docx

./Jean Carew:
Jean Carew - Bal mascat 0.99 '{Dragoste}.docx

./Jean Cayrol:
Jean Cayrol - Adevarurile Catherinei 2.0 '{Dragoste}.docx
Jean Cayrol - Sa nu uitati ca ne iubim. Strainele corpuri 1.0 '{Dragoste}.docx

./Jean Christophe Grange:
Jean Christophe Grange - Diamante mortale 1.0 '{Thriller}.docx
Jean Christophe Grange - Kaiken 1.0 '{Thriller}.docx
Jean Christophe Grange - Pasagerul 1.0 '{Thriller}.docx

./Jean Christophe Rufin:
Jean Christophe Rufin - Spanzuratul de la Conakry 1.0 '{Politista}.docx

./Jean Claude Carriere:
Jean Claude Carriere - Cercul mincinosilor 1.0 '{Filozofie}.docx

./Jean Claud Larchet:
Jean Claud Larchet - Terapeutica bolilor spirituale 1.0 '{Spiritualitate}.docx

./Jean Daniel Baltassat:
Jean Daniel Baltassat - Tainele donei Isabel 1.0 '{Literatura}.docx

./Jean Davidson:
Jean Davidson - Magicul Manhattan 0.99 '{Dragoste}.docx

./Jean de Cars:
Jean de Cars - Povestea Vienei 1.0 '{Istorie}.docx

./Jean de la Hire:
Jean de la Hire - Cei Trei Cercetasi - V01 La voia intamplarii! 2.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V02 Bratul de sange 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V03 Tara groazei 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V04 Boturile rosii 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V05 Lupta de la Kasba 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V06 Cecul de 5 milioane 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V07 Rebeli din tara apasilor 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V08 Misterul lacului sacru 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V09 Cercurile de foc 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V10 Valtorile cataractei 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V11 Lupta navala 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V12 Dervisii asasini 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V13 Goeleta fatala 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V14 Pluta ratacita 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V15 Jungla tragica 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V16 Secta strangulatorilor 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V17 Piratii din Tonkin 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V18 Pagoda insufletita 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V19 Mina ratacita 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V20 Conspiratia indusilor 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V21 Creatorul de monstri 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V22 Tezaurul ocnasilor 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V23 Ultimul deportat 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V24 Tunelul infernal 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V25 Un 'palace' in ghetari 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V26 Insula pastelui 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V27 Glasul din stanca 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V28 Prizoniera Patagonilor 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V29 Stramtoarea Magellan 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V30 Demonul pampasului 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V31 Calul instelat 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V46 Pelerinele negre 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V47 Sania infernala 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V48 Masina de taiat capetele 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V49 Tangoul lui Zomba 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V50 Atacul aerian 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V71 Clopotul din padurea misterioasa 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V72 Urmele de sange 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V73 Indienii Puelsci 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V74 Devastatorii Amazonului 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V75 Ferocii Mangasi 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V76 Dansul scalpului 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V77 Insula fortificata 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V78 Fiarele si mangasii 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V79 Omul misterios 1.0 '{Tineret}.docx
Jean de la Hire - Cei Trei Cercetasi - V80 Carnetul rosu 1.0 '{Tineret}.docx

./Jean de Letraz:
Jean de Letraz - 12 nopti de dragoste 1.0 '{Literatura}.docx

./Jean Dormesson:
Jean Dormesson - Vama marii 0.7 '{Diverse}.docx

./Jean Dutourd:
Jean Dutourd - Primavara vietii 1.0 '{Dragoste}.docx

./Jeane Richards:
Jeane Richards - Ratiunile inimii 0.99 '{Romance}.docx

./Jeanette Winterson:
Jeanette Winterson - Pasiunea 0.99 '{AventuraIstorica}.docx
Jeanette Winterson - Portocalele nu sunt singurele fructe 0.99 '{Romance}.docx
Jeanette Winterson - Sexul ciresilor 1.0 '{Literatura}.docx

./Jeane Wallace:
Jeane Wallace - In barlogul corsarilor 0.99 '{Dragoste}.docx

./Jean Francois Soulet:
Jean Francois Soulet - Istoria comparata a statelor comuniste 2.0 '{Istorie}.docx

./Jean Giono:
Jean Giono - Sa-mi ramana bucuria 0.6 '{Dragoste}.docx
Jean Giono - Turma 1.0 '{Literatura}.docx

./Jean Guitton:
Jean Guitton - Dumnezeu si stiinta 1.0 '{Spiritualitate}.docx

./Jeanic Steele:
Jeanic Steele - Lectura si scrierea pentru dezvoltarea gandirii critice V1 0.8 '{Diverse}.docx

./Jeaniene Frost:
Jeaniene Frost - La jumatatea drumului spre mormant 1.0 '{Vampiri}.docx

./Jean Joseph Renaud:
Jean Joseph Renaud - Documentul 127 1.0 '{Politista}.docx

./Jean Korelitz:
Jean Korelitz - Ar fi trebuit sa stii 1.0 '{Literatura}.docx

./Jean Lorin Sterian:
Jean Lorin Sterian - Clopotele bat fara noima 0.9 '{SF}.docx
Jean Lorin Sterian - Stapanii abstracti ai lumii 0.9 '{SF}.docx

./Jean Louis Bouquet:
Jean Louis Bouquet - Alouqua sau comedia mortilor 1.0 '{Diverse}.docx

./Jean Louis Lafitte:
Jean Louis Lafitte - Dezertorul 1.0 '{ActiuneComando}.docx

./Jean Marie Gustave:
Jean Marie Gustave - Potopul 0.7 '{SF}.docx
Jean Marie Gustave - Urania 0.8 '{SF}.docx

./Jean Marie Rouart:
Jean Marie Rouart - Bernis cardinalul placerilor 0.7 '{Diverse}.docx

./Jeanne Kalogridis:
Jeanne Kalogridis - Mireasa in familia Borgia 0.9 '{Diverse}.docx

./Jeannette Walls:
Jeannette Walls - Castelul de sticla 1.0 '{Literatura}.docx

./Jeanne Worley:
Jeanne Worley - Singuri in jungla 0.9 '{Romance}.docx

./Jean Norman:
Jean Norman - Misterioasa explozie de pe Nicobar 1.0 '{Tineret}.docx

./Jean Orieux:
Jean Orieux - Sfinxul neinteles 0.5 '{Diverse}.docx

./Jean Palou:
Jean Palou - Vrajitoria 0.7 '{Spiritualitate}.docx

./Jean Patrick Manchette:
Jean Patrick Manchette - Mercenarul. Pe viata si pe moarte 1.0 '{Suspans}.docx

./Jean Paul Roux:
Jean Paul Roux - Regele. Mituri si simboluri 0.7 '{Diverse}.docx

./Jean Paul Sartre:
Jean Paul Sartre - Cu usile inchise 0.99 '{Teatru}.docx
Jean Paul Sartre - Existentialismul este un umanism 0.9 '{Filozofie}.docx
Jean Paul Sartre - Morti fara ingropaciune 0.99 '{Teatru}.docx
Jean Paul Sartre - Mustele 0.99 '{Teatru}.docx
Jean Paul Sartre - Portret la 70 de ani 0.8 '{Convorbiri}.docx
Jean Paul Sartre - Zidul 1.0 '{Filozofie}.docx

./Jean Pendziwol:
Jean Pendziwol - Fiicele paznicului 1.0 '{Literatura}.docx

./Jean Petithug:
Jean Petithug - Secretul incasilor 2.0 '{Tineret}.docx

./Jean Pierre Adrevon:
Jean Pierre Adrevon - Jerold si pisica 0.99 '{SF}.docx
Jean Pierre Adrevon - Operatie de rutina 0.99 '{SF}.docx

./Jean Pierre Planque:
Jean Pierre Planque - Din tata necunoscut 0.9 '{SF}.docx

./Jean Plaidy:
Jean Plaidy - Lucrezia Borgia. Pacate din iubire 0.9 '{Literatura}.docx

./Jean Ray:
Jean Ray - Adevarul asupra unchiului Timotheus 0.99 '{SF}.docx

./Jean S. Macleod:
Jean S. Macleod - In inima zapezilor 0.99 '{Romance}.docx

./Jean Saunders:
Jean Saunders - Calatorie in Franta 0.99 '{Dragoste}.docx
Jean Saunders - Jurnalul Ninei 0.9 '{Romance}.docx

./Jean Servier:
Jean Servier - Magia 0.9 '{Spiritualitate}.docx
Jean Servier - Terorismul 0.99 '{Politica}.docx

./Jean Ure:
Jean Ure - O simfonie pentru tine 0.99 '{Dragoste}.docx

./Jeanure:
Jeanure - Zori de zi 0.9 '{Dragoste}.docx

./Jean Voussac:
Jean Voussac - Capitanul Uragan 1.0 '{Tineret}.docx

./Jeet Thayil:
Jeet Thayil - Narcopolis 0.99 '{Literatura}.docx

./Jeff Carlson:
Jeff Carlson - Privire in viitor 0.99 '{SF}.docx

./Jeffery Deaver:
Jeffery Deaver - Gradina fiarelor 2.0 '{Politista}.docx
Jeffery Deaver - Kathryn Dance - V1 Papusa adormita 1.0 '{Politista}.docx
Jeffery Deaver - Kathryn Dance - V2 Moartea vine on line 1.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V1 Colectionarul de oase 2.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V2 Dansand cu moartea 1.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V3 Scaunul gol 2.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V4 Maimuta de piatra 2.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V5 Omul disparut 2.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V6 A douasprezecea carte 1.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V7 Luna rece 1.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V8 Fereastra sparta 1.0 '{Politista}.docx
Jeffery Deaver - Lincoln Rhymes - V10 Camera ucigasa 1.0 '{Politista}.docx

./Jeff Lindsay:
Jeff Lindsay - Dexter - V1 Vise intunecate 1.0 '{Politista}.docx
Jeff Lindsay - Dexter - V2 Dragul si devotatul Dexter 1.0 '{Politista}.docx

./Jeffrey Archer:
Jeffrey Archer - A 11-a porunca 1.0 '{Politista}.docx
Jeffrey Archer - Aici se ascunde o poveste 1.0 '{Politista}.docx
Jeffrey Archer - A patra putere 1.0 '{Politista}.docx
Jeffrey Archer - Cain si Abel 2.0 '{Politista}.docx
Jeffrey Archer - Cap sau pajura 1.0 '{Politista}.docx
Jeffrey Archer - Cararile gloriei 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V1 Doar timpul ne va spune 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V2 Pacatele tatalui 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V3 Un secret bine pastrat 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V4 Ai grija ce-ti doresti 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V5 Mai puternic decat sabia 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V6 A venit vremea 1.0 '{Politista}.docx
Jeffrey Archer - Cronicile Familiei Clifton - V7 A fost un om 1.0 '{Politista}.docx
Jeffrey Archer - Evanghelia dupa Iuda 1.0 '{Spiritualitate}.docx
Jeffrey Archer - Fara drept de apel 1.0 '{Politista}.docx
Jeffrey Archer - Fiica risipitoare 2.0 '{Politista}.docx
Jeffrey Archer - Fiii norocului 1.0 '{Politista}.docx
Jeffrey Archer - Hoti de onoare 1.0 '{Politista}.docx
Jeffrey Archer - Impresie falsa 1.0 '{Politista}.docx
Jeffrey Archer - In linie dreapta 1.0 '{Politista}.docx
Jeffrey Archer - Jurnalul unui lord intemnitat - V1 Belmarsh, iadul 1.0 '{Politista}.docx
Jeffrey Archer - Nici un ban in plus, nici un ban in minus 2.0 '{Politista}.docx
Jeffrey Archer - O chestiune de onoare 1.0 '{Politista}.docx
Jeffrey Archer - O duzina de tertipuri 1.0 '{Politista}.docx
Jeffrey Archer - O tolba plina cu sageti 2.1 '{Politista}.docx
Jeffrey Archer - Pisica are noua vieti 1.0 '{Politista}.docx
Jeffrey Archer - Povestiri cu final nesteptat 1.0 '{Politista}.docx
Jeffrey Archer - Primul intre egali 1.0 '{Politista}.docx
Jeffrey Archer - Sa-i spunem presedintelui 2.0 '{Politista}.docx
Jeffrey Archer - Sa scurtam povestea 1.0 '{Politista}.docx
Jeffrey Archer - Spune povestea 1.0 '{Politista}.docx

./Jeffrey Fox:
Jeffrey Fox - Omul care aduce ploaia 1.0 '{DezvoltarePersonala}.docx

./Jeff Vandermeer:
Jeff Vandermeer - Predator - Teroare in jungla 1.0 '{SF}.docx

./Jeff VanderMeer:
Jeff VanderMeer - Southern Reach - V1 Anihilare 1.0 '{ActiuneComando}.docx
Jeff VanderMeer - Southern Reach - V2 Autoritate 1.0 '{ActiuneComando}.docx
Jeff VanderMeer - Southern Reach - V3 Acceptare 1.0 '{ActiuneComando}.docx

./Jeff Wehr:
Jeff Wehr - Apocalipsa lui Isus Hristos 0.8 '{Spiritualitate}.docx

./Jeni Acterian:
Jeni Acterian - Jurnalul unei fete greu de multumit 1.0 '{Jurnal}.docx

./Jenna Evans Welch:
Jenna Evans Welch - Love & Gelato. Vacanta la Florenta 1.0 '{Dragoste}.docx
Jenna Evans Welch - Love & Luck. O calatorie norocoasa prin Irlanda 1.0 '{Dragoste}.docx

./Jennifer Ames:
Jennifer Ames - Razbunare ratata 1.0 '{Romance}.docx

./Jennifer Blake:
Jennifer Blake - Cele Trei Gratii - V1 Rasplata unui rege 1.0 '{Romance}.docx
Jennifer Blake - Cele Trei Gratii - V2 Capcana inimii 1.0 '{Romance}.docx
Jennifer Blake - Cele Trei Gratii - V3 Cavalerul de aur 1.0 '{Romance}.docx
Jennifer Blake - Duelul inimilor 2.0 '{Romance}.docx
Jennifer Blake - Iasomie salbatica 1.0 '{Romance}.docx

./Jennifer Brozek:
Jennifer Brozek - Ultimul Raport 1.0 '{SF}.docx

./Jennifer E. Smith:
Jennifer E. Smith - Probabilitatea statistica 1.0 '{Tineret}.docx
Jennifer E. Smith - Salut, ramas bun si tot ce se intampla intre ele 1.0 '{Dragoste}.docx
Jennifer E. Smith - Uite asa arata fericirea 0.99 '{Tineret}.docx

./Jennifer Epstein:
Jennifer Epstein - Zeii pedepselor ceresti 0.9 '{Literatura}.docx

./Jennifer L. Armentrout:
Jennifer L. Armentrout - Lux - V1 Obsidian 1.0 '{SF}.docx
Jennifer L. Armentrout - Lux - V2 Onix 1.0 '{SF}.docx
Jennifer L. Armentrout - Lux - V3 Opal 1.0 '{SF}.docx
Jennifer L. Armentrout - Ramai cu mine 0.99 '{Diverse}.docx
Jennifer L. Armentrout - Te astept 0.99 '{Diverse}.docx
Jennifer L. Armentrout - Te doresc 0.99 '{Diverse}.docx
Jennifer L. Armentrout - Titanii - V2 Puterea 0.99 '{Erotic}.docx

./Jennifer Lee Carrell:
Jennifer Lee Carrell - Codul Shakespeare 1.0 '{Diverse}.docx

./Jennifer Niven:
Jennifer Niven - Toate acele locuri minunate 1.0 '{Literatura}.docx

./Jennifer Williams:
Jennifer Williams - De nepretuit 0.8 '{Dragoste}.docx

./Jenny Hubbard:
Jenny Hubbard - Zapada mieilor 1.0 '{Tineret}.docx

./Jenny Randles:
Jenny Randles - Calatoria in timp - fapte, dovezi, posibilitati 0.8 '{MistersiStiinta}.docx
Jenny Randles - Copiii din stele 1.0 '{MistersiStiinta}.docx

./Jenny Ranger:
Jenny Ranger - Ferma 0.99 '{Dragoste}.docx
Jenny Ranger - Mosia arborilor de piper 0.99 '{Dragoste}.docx
Jenny Ranger - Sirena cu flori de iasomie 0.99 '{Dragoste}.docx

./Jens Lapidus:
Jens Lapidus - Trilogia neagra a Stockholmului - V4 Salonul Vip 1.0 '{Politista}.docx
Jens Lapidus - Trilogia neagra a Stockholmului - V5 Stockholm Delete 1.0 '{Politista}.docx

./Jercan Danut Alexandru:
Jercan Danut Alexandru - Exista sau nu exista 0.99 '{Versuri}.docx

./Jeremi Sauvage:
Jeremi Sauvage - Fulgi de zapada eterna 0.9 '{SF}.docx
Jeremi Sauvage - Manus Dei 0.9 '{SF}.docx

./Jeremy Robinson:
Jeremy Robinson - Ultima misiune a Generalului Stonewall 1.0 '{SF}.docx

./Jeroen Brouwers:
Jeroen Brouwers - Rosu ucigas 1.0 '{ActiuneComando}.docx

./Jerome Ferrari:
Jerome Ferrari - Predica despre caderea Romei 1.0 '{Literatura}.docx

./Jerome K. Jerome:
Jerome K. Jerome - Arta de a nu scrie un roman 1.0 '{Umor}.docx
Jerome K. Jerome - Gandurile trandave ale unui pierde vara 0.99 '{Umor}.docx
Jerome K. Jerome - Idei trandave 1.0 '{Umor}.docx
Jerome K. Jerome - Tommy si prietenii sai 1.0 '{Umor}.docx
Jerome K. Jerome - Trei intr-o barca 0.9 '{Umor}.docx
Jerome K. Jerome - Trei pe doua biciclete 1.0 '{Umor}.docx

./Jerrold L. & Schecter Leona:
Jerrold L. & Schecter Leona - Pavel Sudoplatov. Misiuni speciale. Arhitectura terorii 2.0 '{Teroare}.docx

./Jerry Bridges:
Jerry Bridges - Pacate respectabile 0.9 '{Spiritualitate}.docx

./Jerry Oltion:
Jerry Oltion - Dupa judecata de apoi 1.0 '{SF}.docx

./Jerry Seinfeld:
Jerry Seinfeld - Pe limba mea 1.0 '{Tineret}.docx

./Jerry Spinelly:
Jerry Spinelly - Copila stea 1.0 '{Tineret}.docx

./Jerzy Edigey:
Jerzy Edigey - Valiza cu milioane 1.0 '{Politista}.docx

./Jessica North:
Jessica North - Banuiala nedrepta 0.9 '{Romance}.docx
Jessica North - Nevasta pe masura 0.99 '{Dragoste}.docx
Jessica North - Pasiune nestinsa 0.99 '{Romance}.docx
Jessica North - Vacanta in Mauritius 0.99 '{Romance}.docx

./Jessica Shattuck:
Jessica Shattuck - Vaduvele de la castel 1.0 '{Diverse}.docx

./Jessica Steele:
Jessica Steele - Cred in tine 1.0 '{Romance}.docx

./Jessie Parker:
Jessie Parker - Copilaria regasita 0.2 '{Dragoste}.docx

./Jess Walter:
Jess Walter - Zero 1.0 '{Diverse}.docx

./Jessy Pior:
Jessy Pior - Acul de cravata 1.0 '{Politista}.docx
Jessy Pior - Enigma balerinei 1.0 '{Politista}.docx

./Jhumpa Lahiri:
Jhumpa Lahiri - Distanta dintre noi 1.0 '{Literatura}.docx
Jhumpa Lahiri - Interpret de maladii 0.9 '{Literatura}.docx
Jhumpa Lahiri - Porecla 1.0 '{Literatura}.docx

./Jiang Rong:
Jiang Rong - Totemul lupului V1 1.0 '{Aventura}.docx
Jiang Rong - Totemul lupului V2 1.0 '{Aventura}.docx

./Jill Alexander Essbaum:
Jill Alexander Essbaum - Casnica 1.0 '{Dragoste}.docx

./Jill Baker:
Jill Baker - Padurea de portocali 0.2 '{Dragoste}.docx

./Jill Barnett:
Jill Barnett - Vrajit 1.0 '{Supranatural}.docx

./Jill Eckersleyd:
Jill Eckersleyd - Copilul anxios. Adolescentul anxios 0.8 '{DezvoltarePersonala}.docx

./Jill Gregory & Karen Tintori:
Jill Gregory & Karen Tintori - Cartea numelor 0.9 '{Diverse}.docx

./Jill Murray:
Jill Murray - Am nevoie de iubirea ta 0.99 '{Dragoste}.docx
Jill Murray - Drum regasit 0.9 '{Romance}.docx

./Jim Blake:
Jim Blake - Dinamita si trotil 1.0 '{Western}.docx

./Jim Butcher:
Jim Butcher - Dosarele Dresden - V1 Nori de furtuna 1.0 '{SF}.docx
Jim Butcher - Dosarele Dresden - V2 Luna nebuna 1.0 '{SF}.docx

./Jim Crace:
Jim Crace - In clipa mortii 0.9 '{Diverse}.docx
Jim Crace - Recolta 1.0 '{Literatura}.docx

./Jim Harrison:
Jim Harrison - Legendele toamnei 1.0 '{Literatura}.docx

./Jimmy Guieu:
Jimmy Guieu - Cavernele de pe Wolf 0.99 '{SF}.docx

./Jim Nisbet:
Jim Nisbet - Codex Siracuza 1.0 '{AventuraIstorica}.docx

./Jim Thompson:
Jim Thompson - Ucigasul din mine 1.0 '{Politista}.docx

./Jim Weine:
Jim Weine - Omul din Mexic 1.0 '{Detectiv}.docx

./Jinny S. Ditzler:
Jinny S. Ditzler - Cel mai bun an 1.0 '{DezvoltarePersonala}.docx

./Jiri Marek:
Jiri Marek - OK 096 nu raspunde 1.0 '{Politista}.docx
Jiri Marek - Panoptic de oameni pacatosi 1.0 '{Politista}.docx
Jiri Marek - Panoptic de vechi intamplari criminalistice 1.0 '{Politista}.docx
Jiri Marek - Unchiul meu Ulise V1 0.9 '{Politista}.docx
Jiri Marek - Unchiul meu Ulise V2 0.9 '{Politista}.docx

./Joachim Fest:
Joachim Fest - In buncar cu Hitler 1.0 '{Istorie}.docx

./Joakim Zander:
Joakim Zander - Inotatorul 1.0 '{ActiuneRazboi}.docx

./Joan Bramsch:
Joan Bramsch - Vraja unei clipe 1.0 '{Romance}.docx

./Joan Darling:
Joan Darling - Delfinul alb 0.99 '{Dragoste}.docx
Joan Darling - Voluptatea remuscarii 0.99 '{Dragoste}.docx

./Joan Elliott Pickart:
Joan Elliott Pickart - Doamna de la fereastra 0.99 '{Dragoste}.docx

./Joan J. Domning:
Joan J. Domning - Furtuna la Pfarr Lake 0.99 '{Dragoste}.docx

./Joan Masters:
Joan Masters - O clipa de manie 0.99 '{Romance}.docx

./Joan Mattheus:
Joan Mattheus - Pasiune, vis sau nebunie 0.99 '{Dragoste}.docx

./Joan Matthews:
Joan Matthews - Sarutari furate 0.9 '{Romance}.docx

./Joanna Trollope:
Joanna Trollope - Nurorile 1.0 '{Literatura}.docx
Joanna Trollope - O alta familie 0.99 '{Literatura}.docx

./Joanne Harris:
Joanne Harris - Chocolat 1.0 '{Literatura}.docx

./Joan Reeves:
Joan Reeves - Opriti nunta 0.99 '{Romance}.docx

./Joan Smith:
Joan Smith - Domnisoarele din Bath 0.9 '{Dragoste}.docx
Joan Smith - Magia circului 0.2 '{Dragoste}.docx
Joan Smith - Preafrumoasa Laura 0.99 '{Romance}.docx

./Joan Vinge:
Joan Vinge - Pierduti in spatiu 0.9 '{SF}.docx
Joan Vinge - Regina zapezii 1.0 '{SF}.docx

./Jo Beverley:
Jo Beverley - Fericire periculoasa 1.0 '{Romance}.docx

./Joceline Slater:
Joceline Slater - Impotriva curentului 0.99 '{Romance}.docx

./Jocelyne Godard:
Jocelyne Godard - Evantaiul lui Yasumi 1.0 '{Literatura}.docx

./Jo Dexter:
Jo Dexter - Alarma la Nicosia 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Capcana sangeroasa 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Corida la El Paso 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Filiera pachistaneza 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Macel la Monrovia 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Masacru la Kengtung 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Moarte la Venetia 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Raid in Hong Kong 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Razbunarea tigrului 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Teroarea alba 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Tigrii de hartie 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Ucigasii de pe Rio Negro 1.0 '{ActiuneRazboi}.docx
Jo Dexter - Ucigasii strazii 1.0 '{ActiuneRazboi}.docx

./Jodi Picoult:
Jodi Picoult - Al zecelea cerc 1.0 '{Literatura}.docx
Jodi Picoult - Disparitii 1.0 '{Literatura}.docx
Jodi Picoult - Mici lucruri marete 1.0 '{Literatura}.docx
Jodi Picoult - Nouasprezece minute 1.0 '{Literatura}.docx
Jodi Picoult - O scanteie de viata 1.0 '{Literatura}.docx
Jodi Picoult - O viata de rezerva 1.0 '{Literatura}.docx
Jodi Picoult - Pactul 1.0 '{Literatura}.docx
Jodi Picoult - Povestitorul 1.0 '{Literatura}.docx
Jodi Picoult - Vremea plecarii 1.0 '{Literatura}.docx

./Jo Donnell:
Jo Donnell - Calaretul singuratic 0.99 '{Dragoste}.docx
Jo Donnell - Dincolo de tacerea lor 0.99 '{Dragoste}.docx

./Joe Abercrombie:
Joe Abercrombie - Dulce razbunare 1.0 '{SF}.docx
Joe Abercrombie - Marea Sfaramata - V1 Jumatate de rege 1.0 '{SF}.docx
Joe Abercrombie - Marea Sfaramata - V2 Jumatate de lume 1.0 '{SF}.docx
Joe Abercrombie - Marea Sfaramata - V3 Jumatate de razboi 1.0 '{SF}.docx
Joe Abercrombie - Prima Lege - V1 Taisul sabiei 3.1 '{SF}.docx
Joe Abercrombie - Prima Lege - V2 Fara indurare 3.0 '{SF}.docx
Joe Abercrombie - Prima Lege - V3 Puterea armelor 2.0 '{SF}.docx

./Joe Alex:
Joe Alex - Acolo unde e stapana primavara 0.9 '{Politista}.docx

./Joeann Benoit:
Joeann Benoit - Aschia nu sare departe de trunchi 0.8 '{Psihologie}.docx

./Joe Dispenza:
Joe Dispenza - Distruge-ti obiceiurile nocive 1.0 '{DezvoltarePersonala}.docx

./Joe Haldeman:
Joe Haldeman - Camuflaj 2.0 '{SF}.docx
Joe Haldeman - Conexiunea Psi 1.0 '{SF}.docx
Joe Haldeman - Razboiul etern & Pacea eterna 1.0 '{SF}.docx
Joe Haldeman - Un ragaz pentru a trai 1.0 '{SF}.docx
Joe Haldeman - V1 Razboi etern 0.9 '{CalatorieinTimp}.docx
Joe Haldeman - V2 Pace eterna 2.0 '{CalatorieinTimp}.docx

./Joe Hill:
Joe Hill - Cutia cu fantome 2.0 '{Horror}.docx

./Joe Lansdale:
Joe Lansdale - Iulie insangerat 1.0 '{Politista}.docx

./Joel C. Rosenberg:
Joel C. Rosenberg - Curba terorii 1.0 '{Thriller}.docx
Joel C. Rosenberg - Ultimul jihad 1.0 '{Thriller}.docx

./Joelle Wintrebert:
Joelle Wintrebert - Pruncul oglinda 0.9 '{Diverse}.docx

./Joe Navarro:
Joe Navarro - Secretele comunicarii nonverbale 1.0 '{DezvoltarePersonala}.docx

./Joe Schwarcz:
Joe Schwarcz - Stiinta intre adevar si aberatie 2.0 '{MistersiStiinta}.docx

./Joey Goebel:
Joey Goebel - Anomalii 0.7 '{Diverse}.docx

./Joey Light:
Joey Light - Saruta-ma 0.99 '{Dragoste}.docx

./Jo Franklin:
Jo Franklin - Aripa soimului 0.8 '{Diverse}.docx

./Johana Gustawsson:
Johana Gustawsson - Emily Roy & Alexis Castells - V1 Block 46 1.0 '{Thriller}.docx

./Johan Bojer:
Johan Bojer - Fascinatia minciunii 1.0 '{Literatura}.docx
Johan Bojer - Foamea cea mare 1.0 '{Literatura}.docx
Johan Bojer - Ultimul viking 1.0 '{Literatura}.docx

./Johanna Lindsey:
Johanna Lindsey - Cand dragostea asteapta 1.0 '{Dragoste}.docx
Johanna Lindsey - Cand inima dicteaza 1.0 '{Dragoste}.docx
Johanna Lindsey - Flacari si gheata 1.0 '{Dragoste}.docx
Johanna Lindsey - Mireasa prizoniera 1.0 '{Dragoste}.docx
Johanna Lindsey - Prizonierul dorintelor mele 1.0 '{Dragoste}.docx

./Johannes von Tepl:
Johannes von Tepl - Plugarul si moartea 1.0 '{Filozofie}.docx

./Johansen Iris:
Johansen Iris - Legenda Dansatorului - V1 Legenda dansatorului 0.6 '{Dragoste}.docx
Johansen Iris - Legenda Dansatorului - V2 Pe urmele dansatorului 0.6 '{Dragoste}.docx
Johansen Iris - Legenda Dansatorului - V3 Fascinatia dansatorului 0.6 '{Dragoste}.docx

./Johan Theorin:
Johan Theorin - Camera intunecata 0.9 '{Thriller}.docx
Johan Theorin - Ecouri de dincolo de noapte 0.99 '{Politista}.docx

./Johan Wolfgang Von Goethe:
Johan Wolfgang Von Goethe - Afinitatile elective 0.8 '{ClasicSt}.docx
Johan Wolfgang Von Goethe - Faust V1 1.0 '{ClasicSt}.docx
Johan Wolfgang Von Goethe - Faust V2 1.0 '{ClasicSt}.docx
Johan Wolfgang Von Goethe - Sarpele verde 0.99 '{ClasicSt}.docx
Johan Wolfgang Von Goethe - Suferintele tanarului Werther 0.99 '{ClasicSt}.docx

./John A. Keel:
John A. Keel - Profetiile Omului Molie 0..8 '{MistersiStiinta}.docx

./John A. Mcguckin:
John A. Mcguckin - Semnele imparatiei 0.9 '{Religie}.docx

./John Ajvide Lindqwist:
John Ajvide Lindqwist - Lasa-ma sa intru 1.0 '{Horror}.docx

./John Arden:
John Arden - Azilul fericit 0.9 '{Teatru}.docx
John Arden - Dansul sergentului Musgrave 0.9 '{Teatru}.docx

./John August & Jane Hamsher:
John August & Jane Hamsher - Nascuti asasini 0.8 '{Diverse}.docx

./John Ball:
John Ball - In arsita noptii 1.0 '{Politista}.docx

./John Barnes:
John Barnes - Contur 0.9 '{Diverse}.docx
John Barnes - Uraganele 2.0 '{SF}.docx

./John Barrow:
John Barrow - Originea universului 1.0 '{MistersiStiinta}.docx

./John Barth:
John Barth - Varieteu pe apa 1.1 '{Diverse}.docx

./John Blanchard:
John Blanchard - Intrebari esentiale 0.7 '{Religie}.docx

./John Boyne:
John Boyne - Baiatul cu pijamale in dungi 1.0 '{Tineret}.docx

./John Braine:
John Braine - Drumul spre inalta societate 0.7 '{Literatura}.docx

./John Breck:
John Breck - Darul sacru al vietii 0.9 '{Religie}.docx

./John Brunner:
John Brunner - Constelatia rebela 2.0 '{SF}.docx
John Brunner - Oile privesc in sus 0.8 '{SF}.docx
John Brunner - Orasul ca un joc de sah 1.0 '{SF}.docx
John Brunner - Orbita periculoasa 1.0 '{SF}.docx
John Brunner - Rabdarea timpului 1.0 '{SF}.docx
John Brunner - Retelele infinitului 1.0 '{SF}.docx
John Brunner - Sa prinzi o stea cazatoare 1.0 '{SF}.docx
John Brunner - Telepatul 1.0 '{SF}.docx
John Brunner - Timpuri nenumarate 1.0 '{SF}.docx
John Brunner - Victimele Novei 1.0 '{SF}.docx
John Brunner - Zanzibar 0.9 '{CalatorieinTimp}.docx

./John Case:
John Case - A opta zi 1.0 '{Thriller}.docx
John Case - Codul genetic 1.0 '{Thriller}.docx
John Case - Intaiul calaret al apocalipsei 1.0 '{Thriller}.docx
John Case - Sindromul 1.0 '{Thriller}.docx

./John Connolly:
John Connolly - Ingerul negru 1.0 '{Thriller}.docx
John Connolly - Nelinistitii 1.0 '{Thriller}.docx
John Connolly - Razbunatorii 1.0 '{Thriller}.docx
John Connolly - Samuel vs Devil - V1 Portile 1.0 '{Literatura}.docx

./John Crowley:
John Crowley - Zapada 1.0 '{SF}.docx

./John D. Salinger:
John D. Salinger - De veghe in lanul de secara 2.0 '{Literatura}.docx

./John Darnton:
John Darnton - Neanderthal 3.0 '{Thriller}.docx

./John Esmyn:
John Esmyn - Intre doua comori 1.0 '{Aventura}.docx

./John Fante:
John Fante - Arturo Bandini - V1 O sa vina si primavara, Bandini 1.0 '{Literatura}.docx
John Fante - Arturo Bandini - V2 Drumul spre Los Angeles 1.0 '{Literatura}.docx
John Fante - Arturo Bandini - V3 Intreaba praful 1.0 '{Literatura}.docx
John Fante - Arturo Bandini - V4 Vise de pe Bunker Hill 1.0 '{Literatura}.docx

./John Ferris:
John Ferris - Comisionul 1.0 '{SF}.docx

./John Fowles:
John Fowles - Colectionarul 2.0 '{Thriller}.docx
John Fowles - Copacul 0.7 '{Thriller}.docx
John Fowles - Daniel Martin 1.0 '{Thriller}.docx
John Fowles - Iubita locotenentului francez 1.0 '{Thriller}.docx
John Fowles - Magicianul 1.1 '{Thriller}.docx
John Fowles - Mantisa 1.0 '{Thriller}.docx
John Fowles - Omida 1.0 '{Thriller}.docx
John Fowles - Turnul de abanos 0.9 '{Thriller}.docx

./John Foxe:
John Foxe - Istoria mondiala a martirilor 0.9 '{Istorie}.docx

./John Gallat:
John Gallat - Filiera turca 1.0 '{ActiuneComando}.docx

./John Galsworthy:
John Galsworthy - Comedia Moderna - V1 Maimuta alba 0.8 '{ClasicSt}.docx
John Galsworthy - Comedia Moderna - V2 Lingura de argint 0.8 '{ClasicSt}.docx
John Galsworthy - Comedia Moderna - V3 Cantecul lebedei 0.8 '{ClasicSt}.docx
John Galsworthy - Dincolo 1.0 '{ClasicSt}.docx
John Galsworthy - Domeniul 1.0 '{ClasicSt}.docx
John Galsworthy - Floarea intunecata 1.0 '{ClasicSt}.docx
John Galsworthy - Forsyte Saga - V1 Proprietarul 1.0 '{ClasicSt}.docx
John Galsworthy - Forsyte Saga - V2 Vara tarzie a unui Forsyte 1.0 '{ClasicSt}.docx
John Galsworthy - Forsyte Saga - V3 Desteptarea de inchiriat 1.0 '{ClasicSt}.docx
John Galsworthy - Iubirile lui Dinny Cherrell V1 1.0 '{ClasicSt}.docx
John Galsworthy - Iubirile lui Dinny Cherrell V2 1.0 '{ClasicSt}.docx
John Galsworthy - Sfarsit de Capitol - V1 In asteptare 1.0 '{ClasicSt}.docx
John Galsworthy - Sfarsit de Capitol - V2 Pustietate in floare 1.0 '{ClasicSt}.docx
John Galsworthy - Sfarsit de Capitol - V3 Dincolo de rau 1.0 '{ClasicSt}.docx

./John Gray:
John Gray - Barbatii sunt de pe Marte, femeile sunt de pe Venus 0.8 '{DezvoltarePersonala}.docx

./John Green:
John Green - Cautand-o pe Alaska 1.0 '{Dragoste}.docx
John Green - Orase de hartie 0.99 '{Dragoste}.docx
John Green - Sub aceeasi stea 1.0 '{Dragoste}.docx

./John Green & Maureen Johnson & Lauren Myracle:
John Green & Maureen Johnson & Lauren Myracle - Fulgi de iubire 1.0 '{Dragoste}.docx

./John Grisham:
John Grisham - Apelul 1.0 '{Thriller}.docx
John Grisham - Asociatul 1.0 '{Thriller}.docx
John Grisham - Avocatul rebel 1.0 '{Thriller}.docx
John Grisham - Avocatul strazii 0.99 '{Thriller}.docx
John Grisham - Camera mortii 1.0 '{Thriller}.docx
John Grisham - Campionul din Arkansas 1.0 '{Thriller}.docx
John Grisham - Cazul Pelican 2.0 '{Thriller}.docx
John Grisham - Clientul 2.0 '{Thriller}.docx
John Grisham - Firma 0.99 '{Thriller}.docx
John Grisham - Fotbal si pizza 1.0 '{Thriller}.docx
John Grisham - Fratia 1.0 '{Thriller}.docx
John Grisham - Informatorul 1.0 '{Thriller}.docx
John Grisham - Juriul 0.9 '{Thriller}.docx
John Grisham - Maestrul 1.0 '{Thriller}.docx
John Grisham - Mediatorul 1.0 '{Thriller}.docx
John Grisham - Mostenitorii 1.0 '{Thriller}.docx
John Grisham - Muntele familiei Gray 1.0 '{Thriller}.docx
John Grisham - Negustorul de manuscrise 1.0 '{Thriller}.docx
John Grisham - Nevinovatul 0.8 '{Thriller}.docx
John Grisham - Omul care aduce ploaia 1.0 '{Thriller}.docx
John Grisham - Partenerul 1.2 '{Thriller}.docx
John Grisham - Sirul de platani 1.0 '{Thriller}.docx
John Grisham - Si vreme e ca sa ucizi 2.0 '{Thriller}.docx
John Grisham - Testamentul 1.0 '{Thriller}.docx
John Grisham - Theodore Boone 1.0 '{Thriller}.docx
John Grisham - Ultimul jurat 1.1 '{Thriller}.docx
John Grisham - Un altfel de Craciun 1.0 '{Thriller}.docx

./John Grogan:
John Grogan - Eu si Marley 1.0 '{Literatura}.docx

./John Haught:
John Haught - Stiinta si religie 1.0 '{Filozofie}.docx

./John Hersey:
John Hersey - Hiroshima 0.9 '{Literatura}.docx

./John Irving:
John Irving - A patra mana 1.0 '{Literatura}.docx
John Irving - Hotelul New Hampshire 1.0 '{Literatura}.docx

./John Katzenbach:
John Katzenbach - Analistul 1.0 '{Thriller}.docx
John Katzenbach - Calatorul 1.0 '{Thriller}.docx
John Katzenbach - Omul din umbra 1.0 '{Thriller}.docx
John Katzenbach - Omul nepotrivit 1.0 '{Thriller}.docx
John Katzenbach - Povestea unui nebun 1.0 '{Thriller}.docx
John Katzenbach - Razboiul lui Hart 1.0 '{Thriller}.docx
John Katzenbach - Ziua rafuielii 1.0 '{Thriller}.docx

./John Kendrick:
John Kendrick - Cand vinul este rece 0.9 '{Teatru}.docx

./John Kennedy Toole:
John Kennedy Toole - Biblia de neon 1.0 '{Literatura}.docx
John Kennedy Toole - Conjuratia imbecililor 1.0 '{Literatura}.docx

./John Kessel:
John Kessel - Povesti pentru barbati 0.9 '{Literatura}.docx

./John Knittel:
John Knittel - Abd-el-kader 1.0 '{Literatura}.docx
John Knittel - Bazaltul albastru 1.0 '{Literatura}.docx
John Knittel - El hakim 1.0 '{Literatura}.docx
John Knittel - Via mala 1.0 '{Literatura}.docx

./John Langan:
John Langan - Episodul sapte - Ultima rezistenta impotriva haitei in regatul florilor purpurii 0.9 '{SF}.docx

./John Lawton:
John Lawton - Inspector Troy - V1 Black Out 1.0 '{Thriller}.docx

./John Le Carre:
John Le Carre - Cantecul misiunii 2.0 '{Thriller}.docx
John Le Carre - Cartita 2.0 '{Thriller}.docx
John Le Carre - Casa Rusia 1.0 '{Thriller}.docx
John Le Carre - Cel mai vanat om din lume 1.0 '{Thriller}.docx
John Le Carre - Chemare din taramul mortii 1.0 '{Thriller}.docx
John Le Carre - Experiment secret 1.0 '{Thriller}.docx
John Le Carre - Jocul 1.0 '{Thriller}.docx
John Le Carre - O crima la case mari 1.0 '{Thriller}.docx
John Le Carre - Omul nostru din Panama 0.99 '{Thriller}.docx
John Le Carre - Prietenie absoluta 1.0 '{Thriller}.docx
John Le Carre - Spionul care a iesit din joc 1.0 '{Thriller}.docx
John Le Carre - Tainicul pelerin 1.0 '{Thriller}.docx
John Le Carre - Tinker, Taylor, Soldier, Spy 0.8 '{Thriller}.docx

./John Lescroart:
John Lescroart - Juramantul 1.0 '{Thriller}.docx
John Lescroart - Vinovatia 1.0 '{Thriller}.docx

./John Marrs:
John Marrs - Suflete pereche 1.0 '{Literatura}.docx

./John Naish:
John Naish - Ghidul ipohondrului 0.9 '{DezvoltarePersonala}.docx

./Johnny Alici:
Johnny Alici - Am fost 0.8 '{Nuvele}.docx

./John O'brien:
John O'brien - Leaving Las Vegas 0.9 '{Literatura}.docx

./John O'Farrel:
John O'Farrel - Avertisment - poate sa contina urme de alune 0.99 '{Literatura}.docx

./John P. McAfee:
John P. McAfee - Comando prin ploaia trista 2.0 '{ActiuneComando}.docx

./John Parker:
John Parker - In inima Legiunii Straine 1.0 '{Istorie}.docx

./John Penberty:
John Penberty - A fiinta sau a nu fiinta 0.9 '{Diverse}.docx

./John Perkins:
John Perkins - Confesiunile unui asasin economic 1.0 '{Thriller}.docx

./John Piers:
John Piers - Stupefiante 1.0 '{Detectiv}.docx

./John R. Carling:
John R. Carling - Cetatea blestemata 1.0 '{AventuraIstorica}.docx

./John Ray:
John Ray - Osiris judecatorul lumii de apoi 1.0 '{MistersiStiinta}.docx

./John Rekliffe:
John Rekliffe - Femei, regi, calai 0.7 '{Literatura}.docx

./John Saul:
John Saul - Casa de la rascruce 1.1 '{Horror}.docx
John Saul - Cei neiubiti 1.0 '{Horror}.docx
John Saul - Chinuieste-i pe copii 1.1 '{Horror}.docx
John Saul - Clubul Manhattan 1.0 '{Horror}.docx
John Saul - Cosmarul 1.0 '{Horror}.docx
John Saul - Creatura 2.0 '{Horror}.docx
John Saul - Focul iadului 1.0 '{Horror}.docx
John Saul - Fulgerul negru 1.0 '{Horror}.docx
John Saul - Mana dreapta a diavolului 1.0 '{Horror}.docx
John Saul - Pedeapsa pacatosilor 1.0 '{Horror}.docx
John Saul - Prezenta 1.0 '{Horror}.docx
John Saul - Proiect divin 1.1 '{Horror}.docx
John Saul - Protectorul 1.0 '{Horror}.docx
John Saul - Roiul 1.0 '{Horror}.docx
John Saul - Se arata furia oarba 1.0 '{Horror}.docx
John Saul - Somnambulii 1.0 '{Horror}.docx
John Saul - Strigat in noapte 1.0 '{Horror}.docx
John Saul - Umbra 1.9 '{Horror}.docx
John Saul - Vlastarul mintii 1.0 '{Horror}.docx
John Saul - Vocile 0.8 '{Horror}.docx
John Saul - Vocile raului 1.0 '{Horror}.docx

./John Scalzi:
John Scalzi - Razboiul Batranilor - V1 Razboiul batranilor 2.1 '{SF}.docx
John Scalzi - Razboiul Batranilor - V2 Brigazile fantoma 3.0 '{SF}.docx
John Scalzi - Razboiul Batranilor - V3 Ultima colonie 4.0 '{SF}.docx
John Scalzi - Razboiul Batranilor - V4 Povestea lui Zoe 1.0 '{SF}.docx
John Scalzi - Razboiul Batranilor - V5 Prabusirea Imperiului 1.0 '{SF}.docx
John Scalzi - Semnul haosului 2.0 '{SF}.docx

./John Sheridan le Fanu:
John Sheridan le Fanu - Asediul casei rosii 0.8 '{Literatura}.docx

./John Shirley:
John Shirley - Razboinicul 1.0 '{SF}.docx

./John Steinbeck:
John Steinbeck - Cartierul Tortilla 1.0 '{Literatura}.docx
John Steinbeck - Eu si Charley descoperim America 0.99 '{Literatura}.docx
John Steinbeck - Fructele maniei 1.0 '{Literatura}.docx
John Steinbeck - Iarna vrajbei noastre 1.0 '{Literatura}.docx
John Steinbeck - Jurnal rusesc 0.9 '{Literatura}.docx
John Steinbeck - La rasarit de Eden 4.0 '{Literatura}.docx
John Steinbeck - Pasunile raiului 1.0 '{Literatura}.docx
John Steinbeck - Perla 1.0 '{Literatura}.docx
John Steinbeck - Scurta domnie a lui Pepin al IV-lea 1.0 '{Literatura}.docx
John Steinbeck - Soareci si oameni 5.0 '{Literatura}.docx
John Steinbeck - Strada sardinelor 1.0 '{Literatura}.docx

./John Stephens:
John Stephens - Cartile Inceputului - V1 Atlasul de smarald 1.0 '{Supranatural}.docx
John Stephens - Cartile Inceputului - V2 Cronica focului 1.0 '{Supranatural}.docx

./Johnston McCulley:
Johnston McCulley - Semnul lui Zorro 1.0 '{CapasiSpada}.docx
Johnston McCulley - Zorro loveste din nou! 1.0 '{CapasiSpada}.docx

./John T. Basset:
John T. Basset - Doua luni in iad 1.0 '{ActiuneComando}.docx

./John Thomas:
John Thomas - Un ghid al tineretii fara batranete 0.7 '{Spiritualitate}.docx

./John Trenhaile:
John Trenhaile - Crisalida 1.0 '{Suspans}.docx
John Trenhaile - Legile sangelui 1.0 '{Suspans}.docx
John Trenhaile - Paradise Bay - V1 Unealta raului 1.0 '{Suspans}.docx

./John Twelve Hawks:
John Twelve Hawks - Al Patrulea Taram - V1 Calatorul 2.0 '{Thriller}.docx
John Twelve Hawks - Al Patrulea Taram - V2 Raul intunecat 2.0 '{Thriller}.docx

./John Updike:
John Updike - 0 luna de duminici 0.7 '{Literatura}.docx
John Updike - Centaurul 0.8 '{Literatura}.docx
John Updike - Gertrude si Claudius 1.0 '{Literatura}.docx
John Updike - Rabbit - V1 Fugi, rabbit 2.0 '{Literatura}.docx
John Updike - Rabbit - V2 Intoarcerea lui Rabbit 1.0 '{Literatura}.docx
John Updike - Rabbit - V3 Rabbit e bogat 1.0 '{Literatura}.docx
John Updike - Rabbit - V4 Rabbit se odihneste 1.0 '{Literatura}.docx
John Updike - S 0.8 '{Literatura}.docx
John Updike - Versiunea lui Roger 0.9 '{Literatura}.docx

./John Verdon:
John Verdon - Dave Gurney - V1 Gandeste-te la un numar 1.0 '{Thriller}.docx
John Verdon - Dave Gurney - V2 Inchide ochii strans 1.0 '{Thriller}.docx

./John Virapen:
John Virapen - Efecte secundare. Moartea. Dezvaluiri din interiorul industriei farmaceutice 1.0 '{Sanatate}.docx

./John Williams:
John Williams - Stoner 1.0 '{Literatura}.docx

./John Wyndham:
John Wyndham - Crisalidele 2.0 '{SF}.docx
John Wyndham - Ziua trifidelor 2.0 '{SF}.docx

./John Yudkin:
John Yudkin - Cum ucide zaharul 1.0 '{Sanatate}.docx

./Jojo Moyes:
Jojo Moyes - Dupa ce te-am pierdut 1.0 '{Romance}.docx
Jojo Moyes - Eu si totusi alta 1.0 '{Romance}.docx
Jojo Moyes - Fata pe care ai lasat-o in urma 1.0 '{Literatura}.docx
Jojo Moyes - Inainte sa te cunosc 1.0 '{Romance}.docx
Jojo Moyes - In cautarea unui destin 1.0 '{Romance}.docx
Jojo Moyes - Silver Bay 1.0 '{Romance}.docx
Jojo Moyes - Ultima scrisoare de dragoste 2.0 '{Romance}.docx
Jojo Moyes - Un barbat si o femeie 1.0 '{Romance}.docx
Jojo Moyes - Un bilet pentru Paris 1.0 '{Romance}.docx

./Jokai Mor:
Jokai Mor - Diamantele negre 1.0 '{ClasicSt}.docx
Jokai Mor - Eppur si muove 1.0 '{ClasicSt}.docx
Jokai Mor - Fii omului cu inima de piatra V1 1.0 '{ClasicSt}.docx
Jokai Mor - Fii omului cu inima de piatra V2 1.0 '{ClasicSt}.docx
Jokai Mor - Omul de aur 2.0 '{ClasicSt}.docx
Jokai Mor - Omul de aur V1 1.0 '{ClasicSt}.docx
Jokai Mor - Omul de aur V2 1.0 '{ClasicSt}.docx

./Jona Jeffrey:
Jona Jeffrey - Barbatul venit din munti 0.9 '{Dragoste}.docx
Jona Jeffrey - Oaia neagra a familiei 0.99 '{Dragoste}.docx

./Jonas Jonason:
Jonas Jonason - Asasinul Anders si lumea pe intelesul tuturor 1.0 '{Diverse}.docx

./Jonas Jonasson:
Jonas Jonasson - Barbatul de 100 de ani 1.0 '{Aventura}.docx

./Jonas Karlsson:
Jonas Karlsson - Camera 0.99 '{Literatura}.docx

./Jonathan Black:
Jonathan Black - Istoria sacra 1.0 '{Istorie}.docx
Jonathan Black - Istoria secreta a lumii 1.0 '{Istorie}.docx

./Jonathan Coe:
Jonathan Coe - Casa somnului 2.0 '{Literatura}.docx
Jonathan Coe - Inspaimantatoarea viata personala a lui Maxwell Sim 1.0 '{Literatura}.docx

./Jonathan Kellerman:
Jonathan Kellerman - Obsesie 1.0 '{Politista}.docx

./Jonathan Lethem:
Jonathan Lethem - Orfani in Brooklyn 1.0 '{Thriller}.docx

./Jonathan Littell:
Jonathan Littell - Binevoitoarele 1.0 '{Literatura}.docx

./Jonathan Safran Foer:
Jonathan Safran Foer - Extrem de tare, incredibil de aproape 0.99 '{Literatura}.docx
Jonathan Safran Foer - Iata-ma 1.0 '{Literatura}.docx
Jonathan Safran Foer - Totul este iluminat 1.0 '{Literatura}.docx

./Jonathan Stroud:
Jonathan Stroud - Bartimaeus - V1 Amuleta din Samarkand 2.0 '{Aventura}.docx
Jonathan Stroud - Bartimaeus - V2 Ochiul golemului 2.0 '{Aventura}.docx
Jonathan Stroud - Bartimaeus - V3 Poarta lui Ptolemeu 1.0 '{Aventura}.docx
Jonathan Stroud - Lockwood si Asociatii - V1 Conacul bantuit 1.0 '{Aventura}.docx
Jonathan Stroud - Lockwood si Asociatii - V2 Craniul din biblioteca 1.0 '{Aventura}.docx

./Jonathan Swift:
Jonathan Swift - Calatoriile lui Gulliver 1.0 '{Tineret}.docx

./Jonathan Tropper:
Jonathan Tropper - Aici ne despartim 1.0 '{Literatura}.docx

./Jo Nesbo:
Jo Nesbo - Fiul 1.0 '{Suspans}.docx
Jo Nesbo - Harry Hole - V2 Carabusii 1.0 '{Politista}.docx
Jo Nesbo - Liliacul 1.0 '{Suspans}.docx
Jo Nesbo - Omul de zapada 1.0 '{Suspans}.docx
Jo Nesbo - Sange pe zapada 1.0 '{Suspans}.docx
Jo Nesbo - V1 Fantoma trecutului 1.0 '{Suspans}.docx
Jo Nesbo - V2 Calaul 1.0 '{Suspans}.docx
Jo Nesbo - V3 Steaua diavolului 1.0 '{Suspans}.docx
Jo Nesbo - V4 Mantuitorul 1.0 '{Suspans}.docx

./Jones F. Raymond:
Jones F. Raymond - Luna ucigasa 0.99 '{SF}.docx

./Jon F. Merz:
Jon F. Merz - Ninja 1.0 '{SF}.docx

./Jon Fasman:
Jon Fasman - Biblioteca geografului 1.0 '{AventuraIstorica}.docx

./Jon Kalman Stefansson:
Jon Kalman Stefansson - Intre cer si pamant 1.0 '{Literatura}.docx
Jon Kalman Stefansson - Tristetea ingerilor 1.1 '{Literatura}.docx

./Jon Krakauer:
Jon Krakauer - In aerul rarefiat 1.0 '{Aventura}.docx
Jon Krakauer - In salbaticie 1.0 '{Aventura}.docx

./Jon Mcgregor:
Jon Mcgregor - Daca nimeni nu vorbeste despre lucruri deosebite 0.6 '{Diverse}.docx

./Jordi Sierra I Fabra:
Jordi Sierra I Fabra - Fiicele Furtunii - V1 Enigma Maya 1.0 '{AventuraIstorica}.docx
Jordi Sierra I Fabra - Fiicele Furtunii - V2 Crucea Nilului 1.0 '{AventuraIstorica}.docx
Jordi Sierra I Fabra - Fiicele Furtunii - V3 Al cincilea cristal 1.0 '{AventuraIstorica}.docx

./Jordi Sierra i Fabra:
Jordi Sierra i Fabra - Kafka si papusa calatoare 1.0 '{Literatura}.docx

./Jorge Amado:
Jorge Amado - Cavalerul sperantei 1.0 '{Literatura}.docx
Jorge Amado - Gabriela 1.0 '{Literatura}.docx
Jorge Amado - Tocaia Grande 1.1 '{Literatura}.docx

./Jorge Edwards:
Jorge Edwards - Originea lumii 1.0 '{Literatura}.docx

./Jorge Luis Borges:
Jorge Luis Borges - Aleph 1.0 '{ClasicSt}.docx
Jorge Luis Borges - Artificii 0.99 '{ClasicSt}.docx
Jorge Luis Borges - Intrusa 0.99 '{ClasicSt}.docx
Jorge Luis Borges - Juan Murana 0.99 '{ClasicSt}.docx
Jorge Luis Borges - Memoria lui Shakespeare 0.8 '{ClasicSt}.docx
Jorge Luis Borges - Noaptea darurilor 0.99 '{ClasicSt}.docx
Jorge Luis Borges - Opere V1 1.0 '{ClasicSt}.docx
Jorge Luis Borges - Opere V2 1.0 '{ClasicSt}.docx
Jorge Luis Borges - Opere V3 1.0 '{ClasicSt}.docx
Jorge Luis Borges - Relatarea lui Brodie 0.5 '{ClasicSt}.docx
Jorge Luis Borges - Roza lui Paracelsus 0.9 '{ClasicSt}.docx
Jorge Luis Borges - Sudul 0.9 '{ClasicSt}.docx
Jorge Luis Borges - There are more things 0.9 '{ClasicSt}.docx

./Jorge Molist:
Jorge Molist - Inelul 1.0 '{AventuraIstorica}.docx
Jorge Molist - Intoarcerea catarilor 1.0 '{AventuraIstorica}.docx
Jorge Molist - Regina ascunsa 1.0 '{AventuraIstorica}.docx

./Jorge Zepeda Patterson:
Jorge Zepeda Patterson - Tricoul negru 1.0 '{Literatura}.docx

./Jorn Lier Horst:
Jorn Lier Horst - Cainii de vanatoare 1.0 '{Politista}.docx
Jorn Lier Horst - Casa de vacanta 1.0 '{Politista}.docx

./Jose Carlos Somoza:
Jose Carlos Somoza - Zigzag 1.0 '{SF}.docx

./Jose Donoso:
Jose Donoso - Obscena pasare a noptii 1.0 '{Literatura}.docx

./Josef Haltrich:
Josef Haltrich - Basme populare sasesti din Transilvania 0.9 '{BasmesiPovesti}.docx

./Josef Ignacy Kraszewski:
Josef Ignacy Kraszewski - Contesa Cosel 1.0 '{Dragoste}.docx

./Josef Nesvadba:
Josef Nesvadba - Idiotul din Xeenemuende 1.0 '{SF}.docx
Josef Nesvadba - Ultima arma secreta 0.99 '{SF}.docx

./Josef Skvorecky:
Josef Skvorecky - Tristetea locotenentului Boruvka 1.0 '{Politista}.docx

./Josef Toman:
Josef Toman - Don Juan 1.0 '{ClasicSt}.docx

./Joseph Allen Hynek:
Joseph Allen Hynek - The UFO experience 1.0 '{MistersiStiinta}.docx

./Joseph Conrad:
Joseph Conrad - Hanul lui Almayer 0.9 '{Aventura}.docx
Joseph Conrad - Proscrisul din arhipelag 0.5 '{Aventura}.docx
Joseph Conrad - Tarmul refugiului 1.0 '{Aventura}.docx

./Joseph Delaney:
Joseph Delaney - Cronicile Wardstone - V1 Ucenicul vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V2 Blestemul vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V3 Secretul vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V4 Batalia vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V5 Greseala vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V6 Sacrificiul vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V7 Cosmarul vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V8 Destinul vraciului 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V9 Aliata vraciului - Eu sunt Grimalkin 1.0 '{SF}.docx
Joseph Delaney - Cronicile Wardstone - V10 Sangele vraciului 1.0 '{SF}.docx

./Joseph Finder:
Joseph Finder - Instinct ucigas 1.0 '{Thriller}.docx
Joseph Finder - Paranoia 2.0 '{Thriller}.docx

./Joseph Heller:
Joseph Heller - Catch-22 1.0 '{ActiuneComando}.docx
Joseph Heller - Ora inchiderii 0.99 '{Literatura}.docx

./Josephine Angelini:
Josephine Angelini - Predestinati - V1 Predestinati 1.0 '{Supranatural}.docx
Josephine Angelini - Predestinati - V2 Regatul umbrelor 1.0 '{Supranatural}.docx

./Joseph Kessel:
Joseph Kessel - Echipajul 1.0 '{ActiuneRazboi}.docx
Joseph Kessel - Mermoz V1 2.0 '{Aviatie}.docx
Joseph Kessel - Mermoz V2 2.0 '{Aviatie}.docx

./Joseph Maria Bochenski:
Joseph Maria Bochenski - Manual de intelepciune pentru oamenii de rand 0.9 '{DezvoltarePersonala}.docx

./Joseph Messinger:
Joseph Messinger - Gesturile care ii tradeaza pe politicieni 1.0 '{Politica}.docx

./Joseph Murphy:
Joseph Murphy - Practica gandirii pozitive 0.5 '{DezvoltarePersonala}.docx
Joseph Murphy - Puterea extraordinara a subconstientului tau 0.7 '{DezvoltarePersonala}.docx

./Joseph Roth:
Joseph Roth - Cripta habsburgilor 1.0 '{Literatura}.docx
Joseph Roth - Marsul lui Radetzky 1.0 '{Literatura}.docx

./Joseph Rudyard Kipling:
Joseph Rudyard Kipling - A doua carte a junglei 1.0 '{Tineret}.docx
Joseph Rudyard Kipling - Capitani curajosi 1.0 '{Tineret}.docx
Joseph Rudyard Kipling - Cartea junglei 1.0 '{Tineret}.docx
Joseph Rudyard Kipling - Kim V1 0.99 '{Tineret}.docx
Joseph Rudyard Kipling - Kim V2 0.99 '{Tineret}.docx
Joseph Rudyard Kipling - La bunul plac al vietii 1.0 '{Tineret}.docx
Joseph Rudyard Kipling - Se lasa noaptea 1.0 '{Tineret}.docx
Joseph Rudyard Kipling - Stalky & Co. 1.0 '{Tineret}.docx

./Joseph S. Benner:
Joseph S. Benner - Viata impersonala (Traducere 2007) 0.9 '{Spiritualitate}.docx
Joseph S. Benner - Viata impersonala (Traducere 2019) 1.0 '{Spiritualitate}.docx

./Joseph Smith Flechter:
Joseph Smith Flechter - Omul cu urechea taiata 1.0 '{Politista}.docx

./Jose Rodrigues:
Jose Rodrigues - A saptea pecete 1.0 '{AventuraIstorica}.docx
Jose Rodrigues - Codex 632 1.0 '{AventuraIstorica}.docx
Jose Rodrigues - Formula lui Dumnezeu 1.0 '{AventuraIstorica}.docx

./Jose Saramago:
Jose Saramago - Anul mortii lui Ricardo Reis 1.0 '{Literatura}.docx
Jose Saramago - Caietul 0.8 '{Literatura}.docx
Jose Saramago - Cain 0.9 '{Literatura}.docx
Jose Saramago - Calatoria elefantului 1.0 '{Literatura}.docx
Jose Saramago - Eseu despre orbire 1.0 '{Literatura}.docx
Jose Saramago - Evanghelia dupa Isus 0.6 '{Literatura}.docx
Jose Saramago - Farame de memorii 0.8 '{Literatura}.docx
Jose Saramago - Halebarde 0.9 '{Literatura}.docx
Jose Saramago - Intermitentele mortii 0.9 '{Literatura}.docx
Jose Saramago - Lucarna 0.9 '{Literatura}.docx
Jose Saramago - Manual de pictura si caligrafie 0.8 '{Literatura}.docx
Jose Saramago - Memorialul manastirii 1.0 '{Literatura}.docx
Jose Saramago - Omul duplicat 1.0 '{Literatura}.docx
Jose Saramago - Pestera 1.0 '{Literatura}.docx
Jose Saramago - Pluta de piatra 1.0 '{Literatura}.docx
Jose Saramago - Toate numele 1.0 '{Literatura}.docx

./Jose Silva:
Jose Silva - Autocontrolul prin metoda Silva 0.8 '{Spiritualitate}.docx
Jose Silva - Tu vindecatorul 0.7 '{Spiritualitate}.docx

./Josh Conviser:
Josh Conviser - Echelon 1.0 '{SF}.docx

./Josh Mcdowell:
Josh Mcdowell - Mai mult decat un simplu tamplar 0.9 '{Spiritualitate}.docx

./Josie Bell:
Josie Bell - Fata de pe acoperis 0.99 '{Dragoste}.docx

./Josie King:
Josie King - Dans la nunta ta 0.99 '{Dragoste}.docx

./Jostein Gaarder:
Jostein Gaarder - Ca intr-o oglinda. In chip intunecat 2.0 '{Literatura}.docx
Jostein Gaarder - Castelul din Pirinei 2.0 '{Literatura}.docx
Jostein Gaarder - Fata cu portocale 0.99 '{Literatura}.docx
Jostein Gaarder - Lumea Sofiei 0.99 '{Literatura}.docx

./Jo Walton:
Jo Walton - Printre ceilalti 1.0 '{SF}.docx

./Joyce Carol Oates:
Joyce Carol Oates - Incotro pleci. de unde vii 0.9 '{SF}.docx

./Joyce Wade:
Joyce Wade - Amenintarea trecutului 0.99 '{Dragoste}.docx
Joyce Wade - Simbolul dragostei 0.99 '{Dragoste}.docx

./Jozo Niznansky:
Jozo Niznansky - Fantana dragostei 1.0 '{Dragoste}.docx

./Juan David Morgan:
Juan David Morgan - Tacerea lui Gaudi 1.0 '{AventuraIstorica}.docx

./Juan Gomez Jurado:
Juan Gomez Jurado - Spionul lui Dumnezeu 1.0 '{Thriller}.docx

./Juan Goyanarte:
Juan Goyanarte - Lacul Argentino 1.0 '{Literatura}.docx

./Juan Goytisolo:
Juan Goytisolo - Resac 1.0 '{Literatura}.docx
Juan Goytisolo - Tripticul Raului - V1 Carte de identitate 0.9 '{Literatura}.docx
Juan Goytisolo - Tripticul Raului - V2 Don Julian 0.8 '{Literatura}.docx

./Juan Marse:
Juan Marse - Cozi de soparla 0.7 '{Literatura}.docx
Juan Marse - Vraja Shanghaiului 0.8 '{Literatura}.docx

./Juan Onetti:
Juan Onetti - Si atunci cand 0.99 '{Literatura}.docx

./Juan Paolo Cuenca:
Juan Paolo Cuenca - Singurul final fericit pentru o poveste de dragoste e un accident 1.0 '{Literatura}.docx

./Juan Rulfo:
Juan Rulfo - Pedro Paramo 0.99 '{Literatura}.docx

./Juan Saer:
Juan Saer - Martorul 0.99 '{Literatura}.docx

./Juan Valera:
Juan Valera - Juanita 1.0 '{Dragoste}.docx

./Jude Deveraux:
Jude Deveraux - Cantecul de catifea 1.0 '{Dragoste}.docx
Jude Deveraux - Dimineti de lavanda 0.99 '{Dragoste}.docx
Jude Deveraux - Fecioara 1.0 '{Dragoste}.docx
Jude Deveraux - Femeia falsa 1.0 '{Dragoste}.docx
Jude Deveraux - Femeia pierduta 1.0 '{Dragoste}.docx
Jude Deveraux - Geamana de foc 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V2 Ducesa 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V3 Ispita 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V4 Un cavaler pentru mine 0.9 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V5 Rapirea 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V6 Dulcea mincinoasa 2.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V7 Dorinte 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V8 Printesa 0.9 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V9 Iubire eterna 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V10 Ademenitor ca pacatul 0.9 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V11 Tentativa de seductie 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V12 Puterea dragostei 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V13 Invitatia 1.0 '{Dragoste}.docx
Jude Deveraux - Montgomery Taggert - V14 Valul inalt 1.0 '{Dragoste}.docx
Jude Deveraux - Un cavaler pentru mine 1.0 '{Dragoste}.docx

./Jude Mace:
Jude Mace - Flirt periculos 0.99 '{Dragoste}.docx
Jude Mace - Mai presus de diamante 0.99 '{Dragoste}.docx
Jude Mace - Un pariu riscant 0.99 '{Dragoste}.docx

./Judith Duncan:
Judith Duncan - Strazi de foc 0.9 '{Dragoste}.docx

./Judith Hershner:
Judith Hershner - Fara contraziceri 0.99 '{Dragoste}.docx
Judith Hershner - O scrisoare neprevazuta 0.9 '{Dragoste}.docx

./Judith Maynard:
Judith Maynard - Tabara de vacanta 0.8 '{Dragoste}.docx

./Judith Mcnaught:
Judith Mcnaught - Westmoreland - V1 Regatul viselor 1.0 '{Romance}.docx
Judith Mcnaught - Westmoreland - V2 Whitney, dragostea mea 1.0 '{Romance}.docx
Judith Mcnaught - Westmoreland - V3 Pasiune secreta 1.0 '{Romance}.docx
Judith Mcnaught - Westmoreland - V4 Soaptele noptii 1.0 '{Romance}.docx

./Judith Michael:
Judith Michael - Lozul cel mare 0.7 '{Romance}.docx

./Judith Smith:
Judith Smith - Acuzat de crima 0.99 '{Dragoste}.docx
Judith Smith - Apus de soare 0.99 '{Dragoste}.docx
Judith Smith - Cabana alba 0.9 '{Romance}.docx
Judith Smith - Mai este timp pentru impacare 0.99 '{Dragoste}.docx
Judith Smith - Mandolinele din Montori 0.99 '{Dragoste}.docx
Judith Smith - Pilotul 0.99 '{Dragoste}.docx

./Judson Roberts:
Judson Roberts - Saga Arcului Lung - V1 Razboinicul viking 1.0 '{SF}.docx
Judson Roberts - Saga Arcului Lung - V2 Dragonii marilor 1.0 '{SF}.docx

./Judy Christenberry:
Judy Christenberry - Draga Deborah 0.99 '{Dragoste}.docx

./Judy Gill:
Judy Gill - Acea noapte magica 0.9 '{Dragoste}.docx
Judy Gill - Cantecul sirenei 0.9 '{Dragoste}.docx
Judy Gill - Necunoscutul din parcare 0.99 '{Dragoste}.docx

./Jules Evans:
Jules Evans - Filosofie pentru viata 1.0 '{Diverse}.docx

./Jules Renard:
Jules Renard - Morcoveata 0.9 '{Diverse}.docx

./Jules Verne:
Jules Verne - 20.000 de leghe sub mari 2.1 '{AventuraTineret}.docx
Jules Verne - 800 de leghe pe Amazon 2.1 '{AventuraTineret}.docx
Jules Verne - Agentia Thompson & Co 2.1 '{AventuraTineret}.docx
Jules Verne - Arhipelagul in flacari 2.0 '{AventuraTineret}.docx
Jules Verne - Aventurile a trei rusi si trei englezi in Africa australa 0.7 '{AventuraTineret}.docx
Jules Verne - Burse de calatorie 2.1 '{AventuraTineret}.docx
Jules Verne - Calatorie in Anglia si Scotia 1.0 '{AventuraTineret}.docx
Jules Verne - Cancelarul 0.7 '{AventuraTineret}.docx
Jules Verne - Capitan la 15 ani 2.1 '{AventuraTineret}.docx
Jules Verne - Capitanul Hatteras 2.1 '{AventuraTineret}.docx
Jules Verne - Casa cu aburi 2.1 '{AventuraTineret}.docx
Jules Verne - Castelul din Carpati. Intamplari neobisnuite 2.1 '{AventuraTineret}.docx
Jules Verne - Castelul din Carpati 2.0 '{AventuraTineret}.docx
Jules Verne - Cele cinci sute de milioane ale Begumei. Sarpele de mare 2.1 '{AventuraTineret}.docx
Jules Verne - Cesar Cascabel 2.1 '{AventuraTineret}.docx
Jules Verne - Cinci saptamani in balon 2.1 '{AventuraTineret}.docx
Jules Verne - Claudius Bombarnac. Keraban incapatanatul 2.1 '{AventuraTineret}.docx
Jules Verne - Claudius Bombarnac 1.0 '{AventuraTineret}.docx
Jules Verne - Clovis Dardentor. Secretul lui Wilhelm Storitz 2.1 '{AventuraTineret}.docx
Jules Verne - Clovis Dardentor 1.0 '{AventuraTineret}.docx
Jules Verne - Copiii capitanului Grant V1 2.1 '{AventuraTineret}.docx
Jules Verne - Copiii capitanului Grant V2 2.1 '{AventuraTineret}.docx
Jules Verne - Cristofor Columb 1.0 '{AventuraTineret}.docx
Jules Verne - De la pamant la luna. In jurul lunii 2.0 '{AventuraTineret}.docx
Jules Verne - Doamna Branican 0.7 '{AventuraTineret}.docx
Jules Verne - Doctor Ox 2.1 '{AventuraTineret}.docx
Jules Verne - Doi ani de vacanta 2.1 '{AventuraTineret}.docx
Jules Verne - Drumul Frantei 0.7 '{AventuraTineret}.docx
Jules Verne - Eternul Adam 0.9 '{AventuraTineret}.docx
Jules Verne - Familia fara nume 0.7 '{AventuraTineret}.docx
Jules Verne - Farul de la capatul lumii 1.0 '{AventuraTineret}.docx
Jules Verne - Fratii Kip 0.6 '{AventuraTineret}.docx
Jules Verne - Frritt Flacc 2.0 '{AventuraTineret}.docx
Jules Verne - Goana dupa meteor 0.99 '{AventuraTineret}.docx
Jules Verne - Hector Servadac 2.1 '{AventuraTineret}.docx
Jules Verne - Inchipuirile lui Jean Marie Cabidoulin 1.0 '{AventuraTineret}.docx
Jules Verne - Inconjurul lunii 0.9 '{AventuraTineret}.docx
Jules Verne - Indiile negre. Goana dupa meteor 2.1 '{AventuraTineret}.docx
Jules Verne - In fata steagului 2.0 '{AventuraTineret}.docx
Jules Verne - Insula cu elice 2.1 '{AventuraTineret}.docx
Jules Verne - Insula misterioasa 2.1 V1 '{AventuraTineret}.docx
Jules Verne - Insula misterioasa 2.1 V2 '{AventuraTineret}.docx
Jules Verne - Intamplari neobisnuite 1.0 '{AventuraTineret}.docx
Jules Verne - Invazia marii 1.0 '{AventuraTineret}.docx
Jules Verne - Mathias Sandorf 0.99 '{AventuraTineret}.docx
Jules Verne - Mihail Strogoff 0.7 '{AventuraTineret}.docx
Jules Verne - Minunatul Orinoco 2.1 '{AventuraTineret}.docx
Jules Verne - Naufragiatii de pe Jonathan 0.9 '{AventuraTineret}.docx
Jules Verne - Nord contra sud 2.0 '{AventuraTineret}.docx
Jules Verne - O calatorie spre centrul pamantului 2.1 '{AventuraTineret}.docx
Jules Verne - Ocolul Pamantului in optzeci de zile 2.1 '{AventuraTineret}.docx
Jules Verne - O fantezie a doctorului Ox 0.9 '{AventuraTineret}.docx
Jules Verne - O tragedie in Livonia 0.7 '{AventuraTineret}.docx
Jules Verne - O zi din viata unui ziarist american in anul 2889 1.0 '{AventuraTineret}.docx
Jules Verne - Parisul in secolul XX 2.0 '{AventuraTineret}.docx
Jules Verne - Pilotul de pe Dunare 2.1 '{AventuraTineret}.docx
Jules Verne - Prichindel 2.1 '{AventuraTineret}.docx
Jules Verne - Printre gheturile eterne 2.0 '{AventuraTineret}.docx
Jules Verne - Raza verde 1.0 '{AventuraTineret}.docx
Jules Verne - Robur cuceritorul. Stapanul lumii 2.1 '{AventuraTineret}.docx
Jules Verne - Robur cuceritorul 1.0 '{AventuraTineret}.docx
Jules Verne - Sarpele de mare 1.0 '{AventuraTineret}.docx
Jules Verne - Satul aerian. Inchipuirile lui Jean Marie Cabidoulin 2.1 '{AventuraTineret}.docx
Jules Verne - Scoala Robinsonilor. Raza verde 2.1 '{AventuraTineret}.docx
Jules Verne - Scoala Robinsonilor 1.0 '{AventuraTineret}.docx
Jules Verne - Secretul lui Wilhelm Storitz 1.0 '{AventuraTineret}.docx
Jules Verne - Sfinxul ghetarilor 2.0 '{AventuraTineret}.docx
Jules Verne - Spargatorii blocadei 1.0 '{AventuraTineret}.docx
Jules Verne - Stapanul lumii 1.0 '{AventuraTineret}.docx
Jules Verne - Steaua sudului 2.1 '{AventuraTineret}.docx
Jules Verne - Supravietuitorii cancelarului 0.7 '{AventuraTineret}.docx
Jules Verne - Testamentul unui excentric 2.1 '{AventuraTineret}.docx
Jules Verne - Tinutul blanurilor V1 2.1 '{AventuraTineret}.docx
Jules Verne - Tinutul blanurilor V2 2.1 '{AventuraTineret}.docx
Jules Verne - Uimitoarea aventura a misiunii Barsac 2.1 '{AventuraTineret}.docx
Jules Verne - Uimitoarele aventuri ale unui chinez 2.0 '{AventuraTineret}.docx
Jules Verne - Uimitoarele peripetii ale jupanului Antifer 2.1 '{AventuraTineret}.docx
Jules Verne - Un bilet de loterie. Farul de la capatul lumii 2.1 '{AventuraTineret}.docx
Jules Verne - Un oras plutitor. Spargatorii blocadei. Invazia marii 2.1 '{AventuraTineret}.docx
Jules Verne - Un oras plutitor 1.0 '{AventuraTineret}.docx
Jules Verne - Un pamant cu susu-n jos 0.9 '{AventuraTineret}.docx
Jules Verne - Vulcanul de aur 2.1 '{AventuraTineret}.docx

./Julia Davis:
Julia Davis - Interludiu irlandez 0.99 '{Dragoste}.docx
Julia Davis - Zborul inimii 0.99 '{Dragoste}.docx

./Julia Franck:
Julia Franck - Femeia din amiaza 0.99 '{Literatura}.docx

./Julia Kristeva:
Julia Kristeva - Batranul si lupii 0.99 '{Diverse}.docx

./Julia Laine:
Julia Laine - In amintirea noastra 0.99 '{Romance}.docx

./Julia Navarro:
Julia Navarro - Biblia de lut 1.0 '{AventuraIstorica}.docx
Julia Navarro - Confreria Sfantului Giulgiu 1.0 '{AventuraIstorica}.docx
Julia Navarro - Sangele nevinovatilor 1.0 '{AventuraIstorica}.docx
Julia Navarro - Spune-mi cine sunt 1.0 '{AventuraIstorica}.docx

./Julian Barnes:
Julian Barnes - Cafe au lait 0.9 '{Literatura}.docx
Julian Barnes - Iubire etc. 0.99 '{Literatura}.docx
Julian Barnes - Metroland 1.0 '{Literatura}.docx
Julian Barnes - Scrisori de la Londra 0.99 '{Literatura}.docx
Julian Barnes - Tristeti de lamaie 2.0 '{Literatura}.docx
Julian Barnes - Trois 0.9 '{Literatura}.docx
Julian Barnes - Zgomotul timpului 1.0 '{SF}.docx

./Julie Garwood:
Julie Garwood - Barbatul ideal 1.0 '{Dragoste}.docx
Julie Garwood - Barbatul meu, iubirea mea 1.0 '{Dragoste}.docx
Julie Garwood - Claybornes Brides - V1 Trandafiri pentru ea 0.99 '{Dragoste}.docx
Julie Garwood - Crima din dragoste 1.0 '{Dragoste}.docx
Julie Garwood - Dorinta rebela 2.0 '{Dragoste}.docx
Julie Garwood - Laird's Brides - V1 Mireasa indaratnica 1.0 '{Dragoste}.docx
Julie Garwood - Muzica umbrei 0.99 '{Dragoste}.docx
Julie Garwood - Printul fermecator 1.0 '{Dragoste}.docx
Julie Garwood - Spionii Coroanei - V1 Doamna leului 1.0 '{Dragoste}.docx
Julie Garwood - Spionii Coroanei - V2 Ingerul pazitor 1.0 '{Dragoste}.docx
Julie Garwood - Spionii Coroanei - V3 Darul 1.0 '{Dragoste}.docx

./Julien Green:
Julien Green - Miezul noptii 1.0 '{Literatura}.docx

./Julien Sandrel:
Julien Sandrel - Camera minunilor 1.0 '{Dragoste}.docx

./Juliette Patrice:
Juliette Patrice - O rana atat de adanca 0.7 '{Romance}.docx
Juliette Patrice - Trandafirul de aur 0.99 '{Romance}.docx

./Julio Cortazar:
Julio Cortazar - Armele secrete 0.8 '{Literatura}.docx
Julio Cortazar - Autostrada din sud 0.9 '{Literatura}.docx
Julio Cortazar - Cat o iubim pe Glenda 0.99 '{Literatura}.docx
Julio Cortazar - Cea de departe 0.99 '{Literatura}.docx
Julio Cortazar - Celalalt cer 0.7 '{Literatura}.docx
Julio Cortazar - Domnisoara Cora 0.7 '{Literatura}.docx
Julio Cortazar - Instructiuni pentru John Howel 0.6 '{Literatura}.docx
Julio Cortazar - Toate focurile 0.8 '{Literatura}.docx

./Julio Murillo:
Julio Murillo - Pamant si apa 1.0 '{SF}.docx
Julio Murillo - Shangri-La 1.0 '{SF}.docx

./Julius Andan:
Julius Andan - Lumea de dupa cortina 0.9 '{Politica}.docx

./Junichiro Tanizaki:
Junichiro Tanizaki - Club Gourmet 0.6 '{Diverse}.docx
Junichiro Tanizaki - Moartea lui O-Tsuya 1.0 '{Erotic}.docx

./Jurgen Brater:
Jurgen Brater - Maimuta din tine 1.0 '{MistersiStiinta}.docx

./Jussi Adler Olsen:
Jussi Adler Olsen - Departamentul Q - V1 Camera groazei 2.0 '{Politista}.docx
Jussi Adler Olsen - Departamentul Q - V2 Vanatorii de oameni 1.0 '{Politista}.docx
Jussi Adler Olsen - Departamentul Q - V3 Somnul ratiunii 1.0 '{Politista}.docx

./Justin A. Coates:
Justin A. Coates - Echipa de exterminare 1.0 '{SF}.docx

./Justin Cronin:
Justin Cronin - Transformarea V1 1.0 '{Vampiri}.docx
Justin Cronin - Transformarea V2 1.0 '{Vampiri}.docx

./Justin Scott:
Justin Scott - Ben Abbott - V1 Dincolo de aparente 1.0 '{Aventura}.docx
Justin Scott - Ben Abbott - V2 Petrecerea 1.0 '{Aventura}.docx
Justin Scott - Cei noua dragoni 1.0 '{Aventura}.docx
Justin Scott - Iubire tradata 1.0 '{Aventura}.docx
Justin Scott - Mandria regilor V1 1.0 '{Aventura}.docx
Justin Scott - Mandria regilor V2 1.0 '{Aventura}.docx
Justin Scott - Vaduva dorintei 1.0 '{Aventura}.docx

./Justin Somper:
Justin Somper - Vampiratii - V1 Demonii oceanelor 1.0 '{Vampiri}.docx
Justin Somper - Vampiratii - V2 Adancurile mortii 1.0 '{Vampiri}.docx
Justin Somper - Vampiratii - V3 Vremurile terorii 1.0 '{Vampiri}.docx
```

